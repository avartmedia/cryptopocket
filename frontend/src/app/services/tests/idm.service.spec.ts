import { TestBed, inject } from '@angular/core/testing';

import { IdmService } from '../idm.service';

describe('IdmService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IdmService]
    });
  });

  it('should be created', inject([IdmService], (service: IdmService) => {
    expect(service).toBeTruthy();
  }));
});
