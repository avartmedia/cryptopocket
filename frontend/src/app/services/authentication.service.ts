import {Injectable} from "@angular/core";
import {HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {HttpClientExtension} from "../core/http-client-extension";
import {Cookie} from "ng2-cookies";
import {AppProperties} from "../config/app-properties";
import {BackendApis} from "../config/api-properties";

export interface LoginStateChangedObserver {
  changed(state: boolean): void;
}

@Injectable()
export class AuthenticationService {

  private _observerList: LoginStateChangedObserver[] = [];
  private loggedIn = false;
  private verifyRunning = false;

  constructor(private _http: HttpClientExtension) {
  }

  authenticate(username: string, password: string, rememberToken: boolean): Observable<any> {
    return this._http
      .post(BackendApis.AUTHENTICATION_AUTH, JSON.stringify({
        'username': username,
        'password': password,
        'rememberToken': rememberToken
      }));
  }

  addObserver(observer: LoginStateChangedObserver): boolean {
    this._observerList.push(observer);
    return this.loggedIn;
  }

  verifyToken(): void {
    if (this.verifyRunning == true) {
      return;
    }

    this.verifyRunning = true;

    this._http
      .get(BackendApis.AUTHENTICATION_VERIFY)
      .toPromise()
      .then(res => {
        if (this.loggedIn != true) {
          this._observerList.forEach(o => o.changed(true));
        }
        this.loggedIn = true;
        this.verifyRunning = false;
      })
      .catch(err => {
        this.verifyRunning = false;
        if (err.status === 401) {
          if (this.loggedIn != false) {
            this._observerList.forEach(o => o.changed(false));
          }
          this.loggedIn = false;
          Cookie.delete(AppProperties.TOKEN_COOKIE_KEY);
        } else {
          throw err;
        }
      })
    ;
  }
}
