import {Injectable} from "@angular/core";
import {HttpClientExtension} from "../core/http-client-extension";
import {IdmUser} from "../models/idm-user";
import {Observable} from "rxjs/Observable";
import {BackendApis} from "../config/api-properties";

@Injectable()
export class IdmService {

  constructor(private _http: HttpClientExtension) {
  }

  get(): Observable<IdmUser> {
    return this._http.get(BackendApis.IDM_USER);
  }
}
