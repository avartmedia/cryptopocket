export const BackendApis = {

  BACKEND_BASE: 'http://localhost:8080',

  AUTHENTICATION_AUTH: '/authentication/authenticate',
  AUTHENTICATION_VERIFY: '/authentication/verify',

  IDM_USER: '/idm'
};
