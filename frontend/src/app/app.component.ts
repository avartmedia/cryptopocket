import {Component} from "@angular/core";
import {AuthenticationService, LoginStateChangedObserver} from "./services/authentication.service";
import {IdmUser} from "./models/idm-user";
import {IdmService} from "./services/idm.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements LoginStateChangedObserver {

  loggedIn: boolean;
  idmUser: IdmUser;

  constructor(private _authService: AuthenticationService, private _idmService: IdmService) {
    this.loggedIn = _authService.addObserver(this);

    if (!this.loggedIn) {
      _authService.verifyToken();
    }
  }

  changed(state: boolean): void {
    this.loggedIn = state;

    if (this.loggedIn) {
      this._idmService.get()
        .subscribe(res => {
          this.idmUser = res;
        }, err => {
          throw err;
        });
    }
  }
}
