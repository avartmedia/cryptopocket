import {Component, OnInit} from "@angular/core";
import {IdmUser} from "../../models/idm-user";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  idmUser: IdmUser;

  constructor() {
  }

  ngOnInit() {
  }

}
