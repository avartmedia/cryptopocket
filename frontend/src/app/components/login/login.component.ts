import {Component, OnInit} from "@angular/core";
import {AuthenticationService, LoginStateChangedObserver} from "../../services/authentication.service";
import {Cookie} from "ng2-cookies";
import {AppProperties} from "../../config/app-properties";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials = {
    username: '',
    password: '',
    rememberToken: false
  };

  constructor(private _authService: AuthenticationService) {
  }

  ngOnInit() {
  }

  login() {
    this._authService.authenticate(
      this.credentials.username,
      this.credentials.password,
      this.credentials.rememberToken
    ).subscribe(res => {
      console.log('Successfully logged in for Token ', res.token);
      Cookie.set(AppProperties.TOKEN_COOKIE_KEY, res.token);

      this._authService.verifyToken();
    }, error => {
      // TODO Error
      console.log('Error logging in: ', error);
    });
  }
}
