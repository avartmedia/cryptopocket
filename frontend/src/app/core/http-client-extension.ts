import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Cookie} from "ng2-cookies";
import {AppProperties} from "../config/app-properties";
import {BackendApis} from "../config/api-properties";

@Injectable()
export class HttpClientExtension {

  private defaultUrl = BackendApis.BACKEND_BASE;

  constructor(private _http: HttpClient) {
  }

  post(url: string, body: any): Observable<any> {
    const headers = this.getHeader();

    return this._http.post(this.defaultUrl + url, body, {
      headers: headers
    });
  }

  // TODO With Parameters
  get(url: string): Observable<any> {
    const headers = this.getHeader();

    return this._http.get(this.defaultUrl + url, {
      headers: headers
    });
  }


  private getHeader(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json')

    // Add Token if set in Cookies
    const authToken = Cookie.get(AppProperties.TOKEN_COOKIE_KEY);
    if (authToken) {
      const splitToken = authToken.split(':');
      if (splitToken.length === 2) {
        headers = headers.append('X-Auth-Token', authToken);
      } else if (splitToken.length === 3) {
        const expiresAt = splitToken[2];
        if (new Date(Number(expiresAt)) > new Date()) {
          headers = headers.append('X-Auth-Token', authToken);
        }
      }
    }

    return headers;
  }
}
