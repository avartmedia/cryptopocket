/**
 * Created by Christian on 12.12.2017.
 */

import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {AuthenticationService, LoginStateChangedObserver} from "../services/authentication.service";
import {interval} from "rxjs/observable/interval";
import {IntervalObservable} from "rxjs/observable/IntervalObservable";

@Injectable()
export class AuthGuard implements CanActivate, LoginStateChangedObserver {

  private loggedIn: boolean;
  private initialCheck = false;

  constructor(private _router: Router, private _authService: AuthenticationService) {
    this.loggedIn = _authService.addObserver(this);

    if (!this.loggedIn) {
      this.initialCheck = true;
      _authService.verifyToken();
    }
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    if (!this.loggedIn) {
      if (!this.initialCheck) {
        this._router.navigate(['login']); // TODO Return-URL
        return false;
      } else {
        return new Promise<boolean>((resolve, reject) => {
          // TODO better Solution? -> Stop on Backend-Failure
          const obs = IntervalObservable.create(50)
            .subscribe(res => {
              console.log('Called');
              if (!this.initialCheck) {
                if (!this.loggedIn) {
                  this._router.navigate(['login']); // TODO Return-URL
                }
                resolve(this.loggedIn);
                obs.unsubscribe();
              }
            }, err => {
              reject(err);
              obs.unsubscribe();
            });
        });
      }
    }

    return true;
  }

  changed(state: boolean): void {
    this.loggedIn = state;
    this.initialCheck = false;
  }
}
