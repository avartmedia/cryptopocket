export class IdmUser {

  id: number;
  version: number;

  username: string;
  roles: string[];

  enabled: boolean;
}
