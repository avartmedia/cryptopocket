import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";


import {AppComponent} from "./app.component";
import {HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from "./modules/routing.module";
import {RegisterComponent} from "./components/register/register.component";
import {LoginComponent} from "./components/login/login.component";
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {FormsModule} from "@angular/forms";
import {AuthenticationService} from "./services/authentication.service";
import {HttpClientExtension} from "./core/http-client-extension";
import {AuthGuard} from "./core/auth-guard";
import {SidebarComponent} from "./components/shared/sidebar/sidebar.component";
import {HeaderComponent} from "./components/shared/header/header.component";
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import {IdmService} from "./services/idm.service";


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    DashboardComponent,
    SidebarComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbDropdownModule.forRoot()
  ],
  providers: [
    AuthenticationService,
    IdmService,
    HttpClientExtension,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
