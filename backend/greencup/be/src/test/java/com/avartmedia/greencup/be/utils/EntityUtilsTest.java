package com.avartmedia.greencup.be.utils;

import com.avartmedia.greencup.api.dto.VersionedDTO;
import com.avartmedia.greencup.be.entity.VersionedEntity;
import org.junit.Test;

import javax.persistence.OptimisticLockException;

/**
 * Tests for {@link EntityUtils}
 */
public class EntityUtilsTest {

    @Test(expected = NullPointerException.class)
    public void dtoNull() {
        TestEntity entity = new TestEntity(null, null);
        EntityUtils.checkOptLock(null, entity);
    }

    @Test(expected = NullPointerException.class)
    public void entityNull() {
        TestDTO dto = new TestDTO(null, null);
        EntityUtils.checkOptLock(dto, null);
    }

    @Test
    public void entityWithoutId() {
        TestDTO dto = new TestDTO(null, null);
        TestEntity entity = new TestEntity(null, null);

        EntityUtils.checkOptLock(dto, entity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void dtoWithoutId() {
        TestDTO dto = new TestDTO(null, null);
        TestEntity entity = new TestEntity(1, 1);

        EntityUtils.checkOptLock(dto, entity);
    }

    @Test(expected = IllegalStateException.class)
    public void dtoWithoutVersion() {
        TestDTO dto = new TestDTO(1, null);
        TestEntity entity = new TestEntity(1, 1);

        EntityUtils.checkOptLock(dto, entity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void differentIds() {
        TestDTO dto = new TestDTO(1, 1);
        TestEntity entity = new TestEntity(2, 1);

        EntityUtils.checkOptLock(dto, entity);
    }

    @Test(expected = OptimisticLockException.class)
    public void differentVersion() {
        TestDTO dto = new TestDTO(1, 1);
        TestEntity entity = new TestEntity(1, 2);

        EntityUtils.checkOptLock(dto, entity);
    }

    @Test
    public void valid() {
        TestDTO dto = new TestDTO(1, 1);
        TestEntity entity = new TestEntity(1, 1);

        EntityUtils.checkOptLock(dto, entity);
    }

    private class TestEntity extends VersionedEntity<Integer> {

        private final Integer id;
        private final Integer version;

        public TestEntity(Integer id, Integer version) {
            this.id = id;
            this.version = version;
        }

        @Override
        public Integer getId() {
            return id;
        }

        @Override
        public Integer getVersion() {
            return version;
        }
    }

    private class TestDTO extends VersionedDTO<Integer> {

        private final Integer id;
        private final Integer version;

        public TestDTO(Integer id, Integer version) {
            this.id = id;
            this.version = version;
        }

        @Override
        public Integer getId() {
            return id;
        }

        @Override
        public Integer getVersion() {
            return version;
        }
    }
}
