package com.avartmedia.greencup.be.testclasses.dto;

import com.avartmedia.greencup.api.dto.VersionedDTO;

/**
 * Simple DTO for Test-Cases
 */
public class TestVersionedDTO extends VersionedDTO<Long> {

    private String randomText;

    public String getRandomText() {
        return randomText;
    }

    public void setRandomText(String randomText) {
        this.randomText = randomText;
    }
}
