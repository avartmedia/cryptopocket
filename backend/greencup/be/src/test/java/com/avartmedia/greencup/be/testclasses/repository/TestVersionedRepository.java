package com.avartmedia.greencup.be.testclasses.repository;

import com.avartmedia.greencup.be.testclasses.entity.TestVersionedEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for {@link TestVersionedEntity}
 */
@Repository
public interface TestVersionedRepository extends JpaRepository<TestVersionedEntity, Long> {
}
