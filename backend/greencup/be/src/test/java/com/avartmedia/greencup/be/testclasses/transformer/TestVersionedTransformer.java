package com.avartmedia.greencup.be.testclasses.transformer;

import com.avartmedia.greencup.be.testclasses.dto.TestVersionedDTO;
import com.avartmedia.greencup.be.testclasses.entity.TestVersionedEntity;
import com.avartmedia.greencup.be.utils.EntityUtils;
import com.avartmedia.greencup.be.utils.Transformer;

/**
 * Simple Transformer used for Testing the generic Services.
 */
public class TestVersionedTransformer implements Transformer<TestVersionedDTO, TestVersionedEntity> {

    @Override
    public TestVersionedEntity transform(TestVersionedDTO source) {
        return transform(source, new TestVersionedEntity());
    }

    @Override
    public TestVersionedEntity transform(TestVersionedDTO source, TestVersionedEntity entity) {
        if (entity == null) {
            entity = new TestVersionedEntity();
        }

        entity.setRandomText(source.getRandomText());
        return entity;
    }

    @Override
    public TestVersionedDTO transformInverse(TestVersionedEntity source) {
        return transformInverse(source, new TestVersionedDTO());
    }

    @Override
    public TestVersionedDTO transformInverse(TestVersionedEntity source, TestVersionedDTO dto) {
        if (dto == null) {
            dto = new TestVersionedDTO();
        }

        EntityUtils.transformBaseToDTO(source, dto);
        dto.setRandomText(source.getRandomText());
        return dto;
    }
}
