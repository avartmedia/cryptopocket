package com.avartmedia.greencup.be;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;

/**
 * Base-Class for Tests injecting Services.
 * Configures the Scan-Packages and DataSource for Testing.
 */
@DataJpaTest
@SpringBootTest(classes = InternalBaseBackendTest.TestConfiguration.class)
public abstract class InternalBaseBackendTest {

    @Configuration
    @EntityScan(basePackages = "com.avartmedia.greencup.be.testclasses.entity")
    @EnableJpaRepositories(basePackages = "com.avartmedia.greencup.be.testclasses.repository")
    @ComponentScan(basePackages = {"com.avartmedia.greencup.be"})
    public static class TestConfiguration {

        @Bean
        @ConfigurationProperties("app.datasource")
        public DataSource dataSource() {
            return DataSourceBuilder
                    .create()
                    .url("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE")
                    .driverClassName("org.hibernate.dialect.H2Dialect")
                    .username("sa")
                    .password("")
                    .build();
        }
    }
}
