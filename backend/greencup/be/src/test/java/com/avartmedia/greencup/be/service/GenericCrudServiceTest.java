package com.avartmedia.greencup.be.service;

import com.avartmedia.greencup.be.InternalBaseBackendTest;
import com.avartmedia.greencup.be.testclasses.dto.TestVersionedDTO;
import com.avartmedia.greencup.be.testclasses.entity.TestVersionedEntity;
import com.avartmedia.greencup.be.testclasses.service.TestVersionedCrudService;
import com.avartmedia.greencup.be.testclasses.transformer.TestVersionedTransformer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.OptimisticLockException;

/**
 * Tests {@link GenericCrudServiceImpl} via the basic Implementation
 * {@link com.avartmedia.greencup.be.testclasses.service.TestVersionedCrudService}
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GenericCrudServiceTest extends InternalBaseBackendTest {

    @Autowired
    private TestVersionedCrudService crudService;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void test_save_insertSuccessfully() {
        TestVersionedDTO dto = new TestVersionedDTO();
        dto.setRandomText("Random text");

        // Save the DTO
        Long id = crudService.save(dto);

        // Check if DTO is persisted.
        TestVersionedEntity entity = entityManager.find(TestVersionedEntity.class, id);

        Assert.assertNotNull(entity);
        Assert.assertEquals(entity.getId(), id);
        Assert.assertEquals(entity.getRandomText(), dto.getRandomText());
    }

    @Test
    public void test_save_updateSuccessfully() {
        TestVersionedEntity entity = new TestVersionedEntity();
        entity.setRandomText("Random Text");
        entityManager.persistAndFlush(entity);

        // Get the DTO and update it.
        TestVersionedDTO dto = new TestVersionedTransformer().transformInverse(entity);
        dto.setRandomText("Another random Text");
        crudService.save(dto);

        entityManager.flush();

        // Check Version-Increment and Text
        TestVersionedEntity result = entityManager.find(TestVersionedEntity.class, dto.getId());
        Assert.assertEquals(result.getRandomText(), dto.getRandomText());
        Assert.assertTrue(result.getVersion() == 1);
    }

    @Test(expected = OptimisticLockException.class)
    public void test_save_updateOptLock() {
        TestVersionedEntity entity = new TestVersionedEntity();
        entity.setRandomText("Random Text");
        entityManager.persistAndFlush(entity);

        TestVersionedDTO dto = new TestVersionedTransformer().transformInverse(entity);
        dto.setRandomText("Another random Text");

        // Update Entity
        entity.setRandomText("Causing OptLock");
        entityManager.persistAndFlush(entity);

        // Produce OptLock
        crudService.save(dto);
    }

    @Test
    public void test_delete_success() {
        TestVersionedEntity entity = new TestVersionedEntity();
        entity.setRandomText("Random Text");
        entityManager.persistAndFlush(entity);

        // Get the DTO and delete it.
        TestVersionedDTO dto = new TestVersionedTransformer().transformInverse(entity);
        crudService.delete(dto);

        // Check if Entity is deleted.
        TestVersionedEntity shouldBeNull = entityManager.find(TestVersionedEntity.class, dto.getId());
        Assert.assertNull(shouldBeNull);
    }

    @Test(expected = OptimisticLockException.class)
    public void test_delete_optLock() {
        TestVersionedEntity entity = new TestVersionedEntity();
        entity.setRandomText("Random Text");
        entityManager.persistAndFlush(entity);

        // Get the DTO.
        TestVersionedDTO dto = new TestVersionedTransformer().transformInverse(entity);

        // Update the Entity
        entity.setRandomText("Another random Text");
        entityManager.persistAndFlush(entity);

        // Try to delete, should throw OptLock
        crudService.delete(dto);
    }
}
