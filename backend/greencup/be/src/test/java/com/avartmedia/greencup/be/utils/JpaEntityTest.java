package com.avartmedia.greencup.be.utils;

import com.avartmedia.greencup.be.InternalBaseBackendTest;
import com.avartmedia.greencup.be.testclasses.entity.TestVersionedEntity;
import com.avartmedia.greencup.be.testclasses.repository.TestVersionedRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test for the Basic Jpa-Functions.
 * Validates Version-Updates and Audited-Information are correctly set.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class JpaEntityTest extends InternalBaseBackendTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TestVersionedRepository versionedRepository;

    @Test
    public void test_versioned() {
        TestVersionedEntity versionedEntity = new TestVersionedEntity();
        versionedEntity.setRandomText("aaaaaaaaaaaaaaaa");
        versionedRepository.save(versionedEntity);

        // Flush to update Version
        entityManager.flush();

        TestVersionedEntity result1 = versionedRepository.findOne(versionedEntity.getId());
        if (result1.getVersion() != 0) {
            Assert.fail("Failed comparing Versions from Entity 1. Exptected 0, Actual " + result1.getVersion());
        }

        result1.setRandomText("bbbbbbbbbbbbbb");
        versionedRepository.save(result1);

        // Flush to update Version
        entityManager.flush();

        TestVersionedEntity result2 = versionedRepository.findOne(versionedEntity.getId());
        if (result2.getVersion() != 1) {
            Assert.fail("Failed comparing Versions from Entity 2. Exptected 1, Actual " + result1.getVersion());
        }
    }
}
