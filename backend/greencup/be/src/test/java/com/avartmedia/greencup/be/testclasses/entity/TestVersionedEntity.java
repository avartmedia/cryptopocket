package com.avartmedia.greencup.be.testclasses.entity;

import com.avartmedia.greencup.be.entity.VersionedEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Entity for testing the Version
 */
@Entity
public class TestVersionedEntity extends VersionedEntity<Long> {

    @Id
    @GeneratedValue
    private Long id;

    private String randomText;

    @Override
    public Long getId() {
        return id;
    }

    public String getRandomText() {
        return randomText;
    }

    public void setRandomText(String randomText) {
        this.randomText = randomText;
    }
}
