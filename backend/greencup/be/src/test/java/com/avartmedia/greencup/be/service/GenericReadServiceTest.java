package com.avartmedia.greencup.be.service;

import com.avartmedia.greencup.api.exception.EntityNotFoundException;
import com.avartmedia.greencup.be.InternalBaseBackendTest;
import com.avartmedia.greencup.be.testclasses.dto.TestVersionedDTO;
import com.avartmedia.greencup.be.testclasses.entity.TestVersionedEntity;
import com.avartmedia.greencup.be.testclasses.service.TestVersionedReadService;
import com.avartmedia.greencup.be.testclasses.transformer.TestVersionedTransformer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests {@link GenericReadServiceImpl} via the basic Implementation
 * {@link com.avartmedia.greencup.be.testclasses.service.TestVersionedReadService}
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GenericReadServiceTest extends InternalBaseBackendTest {

    @Autowired
    private TestVersionedReadService readService;

    @Autowired
    private TestEntityManager entityManager;

    @Test(expected = EntityNotFoundException.class)
    public void test_read_notExists() {
        readService.read(-1L);
    }

    @Test
    public void test_read_returnDTO() {
        TestVersionedEntity entity = new TestVersionedEntity();
        entity.setRandomText("Text");

        entityManager.persistAndFlush(entity);
        Long id = entity.getId();

        TestVersionedDTO selfTransformed = new TestVersionedTransformer().transformInverse(entity);
        TestVersionedDTO result = readService.read(id);

        Assert.assertNotNull(result);
        Assert.assertEquals(result.getId(), selfTransformed.getId());
        Assert.assertEquals(result.getRandomText(), selfTransformed.getRandomText());
    }
}
