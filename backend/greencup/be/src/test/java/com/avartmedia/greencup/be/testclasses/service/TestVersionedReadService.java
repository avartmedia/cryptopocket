package com.avartmedia.greencup.be.testclasses.service;

import com.avartmedia.greencup.be.service.GenericReadServiceImpl;
import com.avartmedia.greencup.be.testclasses.dto.TestVersionedDTO;
import com.avartmedia.greencup.be.testclasses.entity.TestVersionedEntity;
import com.avartmedia.greencup.be.testclasses.repository.TestVersionedRepository;
import com.avartmedia.greencup.be.testclasses.transformer.TestVersionedTransformer;
import com.avartmedia.greencup.be.utils.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * Simple Service for {@link GenericReadServiceImpl}
 */
@Service
public class TestVersionedReadService extends GenericReadServiceImpl<Long, TestVersionedDTO, TestVersionedEntity> {

    @Autowired
    private TestVersionedRepository repository;

    @Override
    protected Transformer<TestVersionedDTO, TestVersionedEntity> getTransformer() {
        return new TestVersionedTransformer();
    }

    @Override
    protected Class<TestVersionedEntity> getEntityClazz() {
        return TestVersionedEntity.class;
    }

    @Override
    protected JpaRepository<TestVersionedEntity, Long> getRepository() {
        return repository;
    }
}
