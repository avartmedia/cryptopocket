package com.avartmedia.greencup.be.entity;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * Base-Class for JPA-Entities.
 * Includes the Version for OptLock-Checks.
 */
@MappedSuperclass
public abstract class VersionedEntity<PK> {

    @Version
    private Integer version;

    public abstract PK getId();

    /**
     * @deprecated Id set by JPA
     */
    @Deprecated
    public final void setId(PK id) {
        throw new UnsupportedOperationException("Id can not be set manually.");
    }

    public Integer getVersion() {
        return version;
    }
}
