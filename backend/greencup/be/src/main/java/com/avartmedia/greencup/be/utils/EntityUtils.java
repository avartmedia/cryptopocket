package com.avartmedia.greencup.be.utils;

import com.avartmedia.greencup.api.dto.Versioned;
import com.avartmedia.greencup.api.dto.VersionedDTO;
import com.avartmedia.greencup.be.entity.VersionedEntity;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.OptimisticLockException;
import java.io.Serializable;

/**
 * Util-Class for handling Entities.
 * Eg. OptimisticLocking-Check.
 */
public final class EntityUtils {

    private static final Logger logger = LoggerFactory.getLogger(EntityUtils.class);

    private EntityUtils() {
        // No Instance
    }

    /**
     * Validate the Version between the DTO and the Entity.
     *
     * @throws OptimisticLockException when the DTO is outdated.
     */
    public static void checkOptLock(Versioned dto, VersionedEntity entity) {
        Validate.notNull(dto, "dto has to be set.");
        Validate.notNull(entity, "entity has to be set.");

        // Invariant-Check
        if ((dto.getId() == null) != (dto.getVersion() == null)) {
            throw new IllegalStateException("Id and Version have either both be set or 'null'.");
        }

        if (entity.getId() == null) {
            return;
        }

        if (dto.getId() == null) {
            throw new IllegalArgumentException(dto.getClass().getSimpleName() + " has no Id. EntityId = " + entity.getId());
        }

        if (!dto.getId().equals(entity.getId())) {
            throw new IllegalArgumentException("DTO " + dto.getClass().getSimpleName() + " and Entity " + entity.getClass().getSimpleName() +
                    " have different IDs. DTO = " + dto.getId() + ", Entity = " + entity.getId());
        }

        if (!dto.getVersion().equals(entity.getVersion())) {
            logger.error("OptimisticLocking: DTO-Type = {}, Entity-Type = {}, ID = {}, DTO-Version = {}, Entity-Version = {}.",
                    dto.getClass().getName(), entity.getClass().getName(), entity.getId(), dto.getVersion(), entity.getVersion());
            throw new OptimisticLockException(dto.getClass().getSimpleName() + " is outdated.");
        }
    }

    /**
     * Transforms the Variables from the Base-Entities to the Base-DTO.
     * <br/>Supports: {@link Versioned} - {@link VersionedEntity}
     */
    public static <T extends Serializable> VersionedDTO<T> transformBaseToDTO(VersionedEntity<T> entity, VersionedDTO<T> dto) {
        Validate.notNull(entity, "Entity must be set.");
        Validate.notNull(dto, "Dto must be set.");

        dto.setId(entity.getId());
        dto.setVersion(entity.getVersion());
        return dto;
    }
}
