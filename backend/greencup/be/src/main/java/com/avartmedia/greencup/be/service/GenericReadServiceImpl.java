package com.avartmedia.greencup.be.service;

import com.avartmedia.greencup.api.dto.Versioned;
import com.avartmedia.greencup.api.exception.EntityNotFoundException;
import com.avartmedia.greencup.api.service.GenericReadService;
import com.avartmedia.greencup.be.entity.VersionedEntity;
import com.avartmedia.greencup.be.utils.Transformer;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

/**
 * Generic Implementation of {@link GenericReadService}
 */
public abstract class GenericReadServiceImpl<PK extends Serializable, DTO extends Versioned<PK>, ENTITY extends VersionedEntity<PK>>
        implements GenericReadService<PK, DTO> {

    private static final Logger logger = LoggerFactory.getLogger(GenericReadServiceImpl.class);

    /**
     * @return The DTO-To-Entity-Transformer
     */
    protected abstract Transformer<DTO, ENTITY> getTransformer();

    /**
     * @return the Class of the Entity for better Logging.
     */
    protected abstract Class<ENTITY> getEntityClazz();

    /**
     * @return the JpaRepository for the Service's Entity
     */
    protected abstract JpaRepository<ENTITY, PK> getRepository();

    @Override
    public DTO read(PK id) {
        Validate.notNull(id, "Id must be set.");

        ENTITY entity = loadEntityById(id);
        logger.info("Loaded Entity {} for Id {} and Version {}.", getEntityClazz().getSimpleName(), id, entity.getVersion());

        DTO dto = getTransformer().transformInverse(entity);
        logger.debug("Loaded Entity {} for Id {} and transformed to DTO {}.", getEntityClazz().getSimpleName(), id, dto);

        return dto;
    }

    /**
     * Loads the Entity by its Id.
     */
    protected ENTITY loadEntityById(PK id) {
        Validate.notNull(id, "Id must be set.");

        ENTITY entity = getRepository().findOne(id);
        if (entity == null) {
            throw new EntityNotFoundException(getEntityClazz(), id);
        }

        return entity;
    }
}
