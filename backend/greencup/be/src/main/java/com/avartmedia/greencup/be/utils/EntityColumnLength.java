package com.avartmedia.greencup.be.utils;

/**
 * Defines common Column-Lengths.
 */
public final class EntityColumnLength {

    public static final int USERNAME = 36;
    public static final int EMAIL = 254;
    public static final int PASSWORD = 150;

    public static final int TIMESTAMP = 19;

    public static final int NUMBER = 35;
    public static final int BOOLEAN = 1;

    public static final int INPUT = 100;
    public static final int COMMENT = 255;

    public static final int CODE = 10;
    public static final int CODE_LONG = 20;

    public static final int BLOB = 4000;

    private EntityColumnLength() {
        // No Instance
    }
}
