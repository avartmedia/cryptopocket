package com.avartmedia.greencup.be.service;

import com.avartmedia.greencup.api.dto.Versioned;
import com.avartmedia.greencup.api.exception.InternalServiceException;
import com.avartmedia.greencup.api.service.GenericCrudService;
import com.avartmedia.greencup.be.entity.VersionedEntity;
import com.avartmedia.greencup.be.utils.EntityUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * Generic Implementation for the {@link GenericCrudService}
 */
public abstract class GenericCrudServiceImpl<PK extends Serializable, DTO extends Versioned<PK>, ENTITY extends VersionedEntity<PK>>
        extends GenericReadServiceImpl<PK, DTO, ENTITY> implements GenericCrudService<PK, DTO> {

    private final static Logger logger = LoggerFactory.getLogger(GenericCrudServiceImpl.class);

    @Override
    @Transactional
    public PK save(DTO dto) {
        Validate.notNull(dto, "Dto must be set.");
        validateDtoOnPersist(dto);

        logger.debug("Saving Entity {} for DTO {}.", getEntityClazz().getSimpleName(), dto);

        ENTITY entity;

        if (dto.getId() != null) {
            entity = loadEntityById(dto.getId());
            EntityUtils.checkOptLock(dto, entity);

            entity = getTransformer().transform(dto, entity);
        } else {
            entity = getTransformer().transform(dto);
        }

        getRepository().save(entity);

        logger.info("Persisted Entity {} with Id {} and Version.", getEntityClazz().getSimpleName(), entity.getId(), entity.getVersion());
        return entity.getId();
    }

    @Override
    @Transactional
    public void delete(DTO dto) {
        Validate.notNull(dto, "Dto must be set.");
        Validate.validState(dto.getId() != null, "Id must be set.");

        ENTITY entity = loadEntityById(dto.getId());
        EntityUtils.checkOptLock(dto, entity);

        logger.info("Deleting Entity {} with Id {}.", getEntityClazz().getSimpleName(), entity.getId());
        getRepository().delete(entity);
    }

    /**
     * Custom Validations pre-persist.
     */
    protected void validateDtoOnPersist(DTO dto) throws InternalServiceException {
        // Custom check if all necessary variables are set correctly.
    }
}
