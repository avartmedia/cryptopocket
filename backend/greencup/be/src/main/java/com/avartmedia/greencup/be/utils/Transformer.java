package com.avartmedia.greencup.be.utils;

import com.avartmedia.greencup.api.dto.Versioned;
import com.avartmedia.greencup.be.entity.VersionedEntity;

/**
 * Interface for DTO-To-Entity-Transformers.
 */
public interface Transformer<DTO extends Versioned, ENTITY extends VersionedEntity> {

    ENTITY transform(DTO source);

    ENTITY transform(DTO source, ENTITY entity);

    DTO transformInverse(ENTITY source);

    DTO transformInverse(ENTITY source, DTO dto);
}
