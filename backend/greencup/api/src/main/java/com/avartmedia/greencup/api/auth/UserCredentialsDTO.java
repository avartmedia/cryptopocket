package com.avartmedia.greencup.api.auth;

import java.io.Serializable;

/**
 * DTO for the transfer of credentials (Username and Password)
 */
public class UserCredentialsDTO implements Serializable {
    private static final long serialVersionUID = 8330974187974554302L;

    private final String username;
    private final String password;

    private final boolean rememberToken;

    public UserCredentialsDTO(String username, String password, boolean rememberToken) {
        this.username = username;
        this.password = password;
        this.rememberToken = rememberToken;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isRememberToken() {
        return rememberToken;
    }
}
