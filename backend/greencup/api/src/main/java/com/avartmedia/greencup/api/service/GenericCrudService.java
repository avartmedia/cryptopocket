package com.avartmedia.greencup.api.service;

import com.avartmedia.greencup.api.dto.Versioned;

import java.io.Serializable;

/**
 * Generic Interface for basic CRUD-Methods.
 */
public interface GenericCrudService<PK extends Serializable, DTO extends Versioned<PK>> extends GenericReadService<PK, DTO> {

    /**
     * Persists the given DTO.
     *
     * @param dto the DTO to be saved.
     * @return the Id of the persisted Object.
     */
    PK save(DTO dto);

    /**
     * Deletes the persisted DTO.
     *
     * @param dto the DTO to be deleted.
     */
    void delete(DTO dto);
}
