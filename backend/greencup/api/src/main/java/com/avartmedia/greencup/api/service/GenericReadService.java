package com.avartmedia.greencup.api.service;

import com.avartmedia.greencup.api.dto.Versioned;

import java.io.Serializable;

/**
 * Generic Interface for reading Services.
 */
public interface GenericReadService<PK extends Serializable, DTO extends Versioned<PK>> {

    /**
     * Loads a persisted DTO by its Id,
     */
    DTO read(PK id);
}
