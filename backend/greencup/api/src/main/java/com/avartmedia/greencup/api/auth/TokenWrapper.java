package com.avartmedia.greencup.api.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.Validate;

import java.io.Serializable;

/**
 * Wrapper-Class for Tokens
 */
public class TokenWrapper implements Serializable {
    private static final long serialVersionUID = 7343811814948602512L;

    @JsonProperty("token")
    private final String token;

    public TokenWrapper(String token) {
        this.token = Validate.notEmpty(token, "Token must be set.");
    }

    public String getToken() {
        return token;
    }
}
