package com.avartmedia.greencup.api.exception;

/**
 * Exception used for Select-Statements by a specific Id with no result.
 */
public class EntityNotFoundException extends InternalServiceException {

    public EntityNotFoundException(Class entityClazz, Object id) {
        super("Entity " + entityClazz.getSimpleName() + " for Id " + id.toString() + " not found.");
    }
}
