package com.avartmedia.greencup.api.dto;

import java.io.Serializable;

/**
 * Interface for DTOs supporting Versioning.
 * Used by Services for easy OptLock-Checks.
 */
public interface Versioned<PK extends Serializable> extends Serializable {

    PK getId();

    Integer getVersion();
}
