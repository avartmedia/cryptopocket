package com.avartmedia.greencup.api.dto;

import java.io.Serializable;

/**
 * Base-Class for DTOs representing Entities with OptLock-Support.
 */
public abstract class VersionedDTO<PK extends Serializable> implements Versioned<PK> {
    private static final long serialVersionUID = -6149032081241796382L;

    private PK id;
    private Integer version;

    /**
     * Validates that both Id and Version are set or both are null
     *
     * @throws IllegalStateException if Id or Version is set, but the other is {@code null}
     */
    public final void invariant() {
        if ((getId() == null) != (getVersion() == null)) {
            throw new IllegalStateException("Id and Version have either both be set or 'null'.");
        }
    }

    public PK getId() {
        return id;
    }

    public void setId(PK id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
