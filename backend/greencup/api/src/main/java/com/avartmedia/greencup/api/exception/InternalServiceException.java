package com.avartmedia.greencup.api.exception;

/**
 * Exception for unexptected internal Errors.
 */
public class InternalServiceException extends RuntimeException {

    public InternalServiceException(String s) {
        super(s);
    }

    public InternalServiceException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
