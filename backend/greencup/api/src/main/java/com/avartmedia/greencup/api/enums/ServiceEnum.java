package com.avartmedia.greencup.api.enums;

import java.io.Serializable;

/**
 * Base-Interface for all Enums used by Entities and REST-Calls.
 *
 * @param <T> the serializable Code-Type
 */
public interface ServiceEnum<T extends Serializable> {

    /**
     * Unique Code for an Enum.
     * Used for Database-Values and REST-Mappings.
     * <p>
     * Short Codes have a max. length of 10,
     * Long Codes have max. length of 20.
     *
     * @return the unique Code.
     */
    T getCode();
}
