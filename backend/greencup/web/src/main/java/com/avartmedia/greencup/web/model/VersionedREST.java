package com.avartmedia.greencup.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Base-Class for all Rest-Responses representing Entities with OptLock-Support.
 * Has to be used to allow Services easy OptLock-Checks.
 */
public abstract class VersionedREST<PK> implements Serializable {
    private static final long serialVersionUID = 5360439249172346261L;

    @JsonProperty("id")
    private PK id;

    @JsonProperty("version")
    private Integer version;


    public PK getId() {
        return id;
    }

    public void setId(PK id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
