package com.avartmedia.greencup.web.utils;

import com.avartmedia.greencup.api.dto.Versioned;
import com.avartmedia.greencup.api.dto.VersionedDTO;
import com.avartmedia.greencup.web.model.VersionedREST;
import org.apache.commons.lang3.Validate;

import java.io.Serializable;

/**
 * Util-Class for transforming {@link VersionedREST} and {@link Versioned}
 */
public final class RestTransformerUtils {

    private RestTransformerUtils() {
        // No Instance
    }

    public static <T extends Serializable> VersionedREST<T> dtoToREST(Versioned<T> source, VersionedREST<T> rest) {
        Validate.notNull(source, "Dto must be set.");
        Validate.notNull(rest, "Rest must be set.");

        rest.setId(source.getId());
        rest.setVersion(source.getVersion());
        return rest;
    }

    public static <T extends Serializable> VersionedDTO<T> restToDTO(VersionedREST<T> source, VersionedDTO<T> dto) {
        Validate.notNull(source, "Rest must be set.");
        Validate.notNull(dto, "Dto must be set.");

        dto.setId(source.getId());
        dto.setVersion(source.getVersion());
        return dto;
    }
}
