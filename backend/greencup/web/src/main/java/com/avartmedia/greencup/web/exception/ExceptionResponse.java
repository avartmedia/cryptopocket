package com.avartmedia.greencup.web.exception;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Rest-Response when an Service-Exception is thrown.
 */
public class ExceptionResponse implements Serializable {
    private static final long serialVersionUID = -4159655728483578935L;

    @JsonProperty("status")
    private Integer status;

    @JsonProperty("internalErrorCode")
    private Integer internalErrorCode;

    @JsonProperty("message")
    private String message;

    @JsonProperty("stackTrace")
    private String stackTrace;

    public ExceptionResponse(Integer status, int internalErrorCode, String message, Throwable ex) {
        this.status = status;
        this.internalErrorCode = internalErrorCode;

        this.message = message;
        this.stackTrace = ex != null ? ex.getMessage() : null;
    }
}
