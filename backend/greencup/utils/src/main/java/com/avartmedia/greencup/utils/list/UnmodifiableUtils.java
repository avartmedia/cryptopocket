package com.avartmedia.greencup.utils.list;

import java.util.*;

/**
 * Provides Methods for unmodifiable Collections.
 */
public final class UnmodifiableUtils {

    private UnmodifiableUtils() {
        // No Instance
    }

    @SafeVarargs
    private static <T> List<T> asList(T... items) {
        return Collections.unmodifiableList(Arrays.asList(items));
    }

    @SafeVarargs
    private static <T> Set<T> asSet(T... items) {
        return Collections.unmodifiableSet(new HashSet<>(Arrays.asList(items)));
    }
}
