package com.avartmedia.greencup.utils.enums;

import com.avartmedia.greencup.api.enums.ServiceEnum;

import java.io.Serializable;

/**
 * Utility-Class for {@link ServiceEnum}
 */
public final class ServiceEnumUtils {

    private ServiceEnumUtils() {
        // No Instance
    }

    public static <CODE extends Serializable, TYPE extends ServiceEnum<CODE>, Enum> TYPE getByCode(final CODE code, final Class<TYPE> clazz) {
        if (code == null) {
            return null;
        }

        for (TYPE t : clazz.getEnumConstants()) {
            if (code.equals(t.getCode())) {
                return t;
            }
        }

        throw new IllegalArgumentException("No Enum for Class " + clazz.getSimpleName() + " with Code " + code.toString() + " found.");
    }
}
