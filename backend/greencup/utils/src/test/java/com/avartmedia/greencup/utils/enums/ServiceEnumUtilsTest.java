package com.avartmedia.greencup.utils.enums;

import com.avartmedia.greencup.api.enums.ServiceEnum;
import org.junit.Assert;
import org.junit.Test;

/**
 * Tests {@link ServiceEnumUtils}
 */
public class ServiceEnumUtilsTest {

    @Test
    public void test_getByCode_returnEnum() {
        for (TestEnum toTest : TestEnum.values()) {
            TestEnum result = ServiceEnumUtils.getByCode(toTest.getCode(), TestEnum.class);
            Assert.assertEquals(result, toTest);
        }
    }

    @Test
    public void test_getByCode_null() {
        TestEnum result = ServiceEnumUtils.getByCode(null, TestEnum.class);
        Assert.assertNull(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_getByCode_notFound() {
        ServiceEnumUtils.getByCode(0, TestEnum.class);
    }

    public enum TestEnum implements ServiceEnum<Integer> {
        VALUE_1(1),
        VALUE_2(2),
        VALUE_3(3),;

        private final Integer code;

        TestEnum(Integer code) {
            this.code = code;
        }

        @Override
        public Integer getCode() {
            return code;
        }
    }
}
