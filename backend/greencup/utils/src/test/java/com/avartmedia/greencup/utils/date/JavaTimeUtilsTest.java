package com.avartmedia.greencup.utils.date;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * Tests for {@link JavaTimeUtils}
 */
public class JavaTimeUtilsTest {

    /**
     * Tests if all Methods are 'Null'-Safe
     */
    @Test
    public void nullValues() {
        JavaTimeUtils.toDate((LocalDate) null);
        JavaTimeUtils.toDate((LocalDateTime) null);
        JavaTimeUtils.toLocalDate(null);
        JavaTimeUtils.toLocalDateTime(null);
    }

    @Test
    public void dateToLocalDate() {
        int year = 2015;
        int month = 10;
        int day = 20;

        Date date = createDate(year, month, day);

        LocalDate result = JavaTimeUtils.toLocalDate(date);

        Assert.assertEquals(result.getYear(), year);
        Assert.assertEquals(result.getMonthValue(), month);
        Assert.assertEquals(result.getDayOfMonth(), day);
    }

    @Test
    public void dateToLocalDateTime() {
        int year = 2024;
        int month = 8;
        int day = 24;
        int hour = 22;
        int minute = 14;

        Date date = createDate(year, month, day, hour, minute);

        LocalDateTime result = JavaTimeUtils.toLocalDateTime(date);

        Assert.assertEquals(result.getYear(), year);
        Assert.assertEquals(result.getMonthValue(), month);
        Assert.assertEquals(result.getDayOfMonth(), day);
        Assert.assertEquals(result.getHour(), hour);
        Assert.assertEquals(result.getMinute(), minute);
    }

    @Test
    public void localDateToDate() {
        int year = 2000;
        int month = 4;
        int day = 12;

        LocalDate localDate = LocalDate.of(year, month, day);

        Date result = JavaTimeUtils.toDate(localDate);
        assertDate(result, year, month, day);
    }

    @Test
    public void localDateTimeToDate() {
        int year = 2013;
        int month = 1;
        int day = 13;
        int hour = 11;
        int minute = 28;

        LocalDateTime localDate = LocalDateTime.of(year, month, day, hour, minute);

        Date result = JavaTimeUtils.toDate(localDate);
        assertDate(result, year, month, day, hour, minute);
    }

    private void assertDate(Date dateToCheck, int year, int month, int day) {
        assertDate(dateToCheck, year, month, day, 0, 0);
    }

    private void assertDate(Date dateToCheck, int year, int month, int day, int hour, int minute) {
        month--;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateToCheck);

        Assert.assertEquals(calendar.get(Calendar.YEAR), year);
        Assert.assertEquals(calendar.get(Calendar.MONTH), month);
        Assert.assertEquals(calendar.get(Calendar.DAY_OF_MONTH), day);
        Assert.assertEquals(calendar.get(Calendar.HOUR_OF_DAY), hour);
        Assert.assertEquals(calendar.get(Calendar.MINUTE), minute);
    }

    private Date createDate(int year, int month, int day) {
        return createDate(year, month, day, 0, 0);
    }

    private Date createDate(int year, int month, int day, int hour, int minute) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);

        return calendar.getTime();
    }
}
