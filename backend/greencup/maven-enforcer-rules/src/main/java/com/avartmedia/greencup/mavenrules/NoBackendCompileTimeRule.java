package com.avartmedia.greencup.mavenrules;

import org.apache.maven.enforcer.rule.api.EnforcerRule;
import org.apache.maven.enforcer.rule.api.EnforcerRuleException;
import org.apache.maven.enforcer.rule.api.EnforcerRuleHelper;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.configurator.expression.ExpressionEvaluationException;

import java.util.Arrays;
import java.util.List;

/**
 * Maven-Rule:
 * Backend-Dependencies with Scope 'compile' forbidden - depend only on Scope 'runtime'.
 */
public class NoBackendCompileTimeRule implements EnforcerRule {

    private static final String SCOPE_COMPILE = "compile";

    private static final String BASE_BACKEND_ARTIFACT = "greencup-be";

    private static final List<String> BACKEND_NAME_MARKERS = Arrays.asList("be", "backend", "impl");
    private static final List<String> VALIDATED_GROUP_IDS = Arrays.asList("avartmedia", "mayobirne", "se");

    @Override
    public void execute(EnforcerRuleHelper enforcerRuleHelper) throws EnforcerRuleException {
        Log logger = enforcerRuleHelper.getLog();

        MavenProject mavenProject;
        try {
            mavenProject = (MavenProject) enforcerRuleHelper.evaluate("${project}");
        } catch (ExpressionEvaluationException e) {
            throw new EnforcerRuleException("Maven-Project could not be loaded.", e);
        }

        logger.info(String.format("Validating Dependency for Module %s", mavenProject.getArtifactId()));

        for (Dependency dependency : mavenProject.getDependencies()) {
            // Check only Compile-Time Dependencies
            if (!SCOPE_COMPILE.equals(dependency.getScope())) {
                continue;
            }

            // Check only self implemented Modules
            if (VALIDATED_GROUP_IDS.contains(dependency.getGroupId())) {
                continue;
            }

            // Check if Dependency is a Backend-Module
            boolean backend = isBackend(dependency.getArtifactId());
            if (!backend) {
                continue;
            }

            // Skip Util-Modules
            if (dependency.getArtifactId().contains("util") ||
                    dependency.getArtifactId().contains("core")) {
                logger.info(String.format("Util-Dependency '%s' for Module '%s' allowed.",
                        dependency.getArtifactId(), mavenProject.getArtifactId()));
                continue;
            }

            // Allow this Project's Backend-Module outside this Project
            if (!mavenProject.getArtifactId().startsWith("greencup") &&
                    BASE_BACKEND_ARTIFACT.equals(dependency.getArtifactId())) {
                continue;
            }

            throw new EnforcerRuleException(String.format("Backend-Dependency '%s' in Module '%s' forbidden",
                    dependency.getArtifactId(), mavenProject.getArtifactId()));
        }
    }

    @Override
    public boolean isCacheable() {
        return false;
    }

    @Override
    public boolean isResultValid(EnforcerRule enforcerRule) {
        return false;
    }

    @Override
    public String getCacheId() {
        return null;
    }

    /**
     * Checks if the Module has a backend-typed Name.
     *
     * @param artifactId the ArtifactId to check.
     * @return {@code true} if the Artifact is a Backend.
     */
    private boolean isBackend(String artifactId) {
        for (String backendMarker : BACKEND_NAME_MARKERS) {
            if (artifactId.endsWith(backendMarker)) {
                return true;
            }
        }

        return false;
    }
}
