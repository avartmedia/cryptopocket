# **Greencup**
##### © Avartmedia
## Base-Project for a Spring-Backend
#### Supports Spring-MVC and Spring-JPA

Basic Setup for a multimodule Project using Spring-MVC and JPA

## Features ~
- Generic Services for Reading and Persisting Jpa-Entities
- "Optimistic Locking"-Support:
    - Base-Classes with Version for Jpa-Entites, DTOs and REST-Objects
    - Util-Class for OptLock-Check
    - Supported by the generic Jpa-Services
- Audited Jpa-Entites automatically saving the authenticated User-Info on persist
- Custom Service-Exceptions for UI-Validations and internal Service-Errors
- Interface for Enums with Code (eg. for Database or Frontend-Usage)
- Base-Test automatically authenticating an User.
- Util-Classes for Authentication and Date-Converting

## Setup ~
1) Include Greencup in your Project
2) Set the Module greencup-definitions as your Parent
3) Depend your Submodules on greencups Base-Modules (eg. be -> be)
4) Setup your own Authentication (using Ldap or a simple Database)
5) Create a MVC-Controller for your Authentication
6) Setup your Database-Information
7) Create a Startup-Module with your Spring-Application
8) Configure your Application (scanPackages)
9) Develop your Application-Features
10) Enjoy.

## Additional Info ~
- Greencup includes a Maven-Enforcerrule preventing all Compile-Scoped Backend-Dependencies excluding Util-Modules
- greencup-be/EntityColumnLength defines common Entity-Column lengths for your Jpa-Entities
- greencup-definitions defines most common Dependencies needed