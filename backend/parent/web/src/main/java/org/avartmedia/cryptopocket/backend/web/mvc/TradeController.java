package org.avartmedia.cryptopocket.backend.web.mvc;

import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;
import org.avartmedia.cryptopocket.backend.api.services.TradeService;
import org.avartmedia.cryptopocket.backend.web.models.TradeRest;
import org.avartmedia.cryptopocket.backend.web.models.converter.TradeConverter;
import org.avartmedia.cryptopocket.backend.web.utils.IdmWebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Christian on 14.12.2017.
 */
@Controller
@RequestMapping("/trade")
public class TradeController {

    private final IdmWebUtils idmWebUtils;
    private final TradeService tradeService;

    @Autowired
    public TradeController(IdmWebUtils idmWebUtils, TradeService tradeService) {
        this.idmWebUtils = idmWebUtils;
        this.tradeService = tradeService;
    }

    @Secured(value = Role.USER_CODE)
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity tradeCurrency(@RequestBody TradeRest body) {
        tradeService.tradeBalance(idmWebUtils.getUserId(), TradeConverter.convertToDTO(body));
        return new ResponseEntity(HttpStatus.OK);
    }

    // TODO History
}
