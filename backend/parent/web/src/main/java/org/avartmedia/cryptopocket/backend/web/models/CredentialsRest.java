package org.avartmedia.cryptopocket.backend.web.models;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CredentialsRest implements Serializable {
    private static final long serialVersionUID = 1323099862721923676L;

    @JsonProperty("username")
    private String username;

    @JsonProperty("password")
    private String password;

    @JsonProperty("rememberToken")
    private Boolean rememberToken;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(Boolean rememberToken) {
        this.rememberToken = rememberToken;
    }
}
