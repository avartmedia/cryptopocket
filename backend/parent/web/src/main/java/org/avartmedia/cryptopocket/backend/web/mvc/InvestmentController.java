package org.avartmedia.cryptopocket.backend.web.mvc;

import org.apache.commons.lang3.Validate;
import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;
import org.avartmedia.cryptopocket.backend.api.services.InvestmentService;
import org.avartmedia.cryptopocket.backend.web.models.InvestmentRest;
import org.avartmedia.cryptopocket.backend.web.models.converter.InvestmentConverter;
import org.avartmedia.cryptopocket.backend.web.utils.IdmWebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Christian on 14.12.2017.
 */
@Controller
@RequestMapping("/investment")
public class InvestmentController {

    private final IdmWebUtils idmWebUtils;
    private final InvestmentService investmentService;

    @Autowired
    public InvestmentController(IdmWebUtils idmWebUtils, InvestmentService investmentService) {
        this.idmWebUtils = idmWebUtils;
        this.investmentService = investmentService;
    }

    @Secured(value = Role.USER_CODE)
    @RequestMapping(method = RequestMethod.POST, value = "/invest")
    public ResponseEntity invest(@RequestBody InvestmentRest investment) {
        Validate.notNull(investment.getCurrencyCode(), "CurrencyCode must be set.");
        Validate.notNull(investment.getAmount(), "Amount must be set.");
        Validate.notNull(investment.getInvestmentValue(), "InvestmentValue must be set.");
        Validate.notNull(investment.getPaymentDate(), "PaymentDate must be set.");

        investmentService.createInvestment(idmWebUtils.getUserId(), InvestmentConverter.convertToDTO(investment));
        return new ResponseEntity(HttpStatus.OK);
    }

    @Secured(value = Role.USER_CODE)
    @RequestMapping(method = RequestMethod.POST, value = "/sell")
    public ResponseEntity sell(@RequestBody InvestmentRest investment) {
        Validate.notNull(investment.getCurrencyCode(), "CurrencyCode must be set.");
        Validate.notNull(investment.getAmount(), "Amount must be set.");
        Validate.notNull(investment.getInvestmentValue(), "InvestmentValue must be set.");
        Validate.notNull(investment.getPaymentDate(), "PaymentDate must be set.");

        investmentService.sellInvestment(idmWebUtils.getUserId(), InvestmentConverter.convertToDTO(investment));
        return new ResponseEntity(HttpStatus.OK);
    }
}
