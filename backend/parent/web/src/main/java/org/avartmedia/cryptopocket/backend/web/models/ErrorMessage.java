package org.avartmedia.cryptopocket.backend.web.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ErrorMessage implements Serializable {
    private static final long serialVersionUID = 562012192179665953L;

    @JsonProperty("httpStatus")
    private Integer httpStatus;

    @JsonProperty("errorCode")
    private Integer errorCode;

    @JsonProperty("debugMessage")
    private String debugMessage;

    public ErrorMessage(Integer httpStatus, Integer errorCode, String debugMessage) {
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
        this.debugMessage = debugMessage;
    }
}
