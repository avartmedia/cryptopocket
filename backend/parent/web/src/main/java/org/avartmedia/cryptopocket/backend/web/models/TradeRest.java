package org.avartmedia.cryptopocket.backend.web.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Christian on 14.12.2017.
 */
public class TradeRest implements Serializable {
    private static final long serialVersionUID = -4717254472043603636L;

    @JsonProperty("sourceList")
    private List<TradeSourceRest> sourceList;

    @JsonProperty("targetCurrencyCode")
    private String targetCurrencyCode;

    @JsonProperty("targetCurrencyAmount")
    private BigDecimal targetCurrencyAmount;

    public List<TradeSourceRest> getSourceList() {
        return sourceList;
    }

    public void setSourceList(List<TradeSourceRest> sourceList) {
        this.sourceList = sourceList;
    }

    public String getTargetCurrencyCode() {
        return targetCurrencyCode;
    }

    public void setTargetCurrencyCode(String targetCurrencyCode) {
        this.targetCurrencyCode = targetCurrencyCode;
    }

    public BigDecimal getTargetCurrencyAmount() {
        return targetCurrencyAmount;
    }

    public void setTargetCurrencyAmount(BigDecimal targetCurrencyAmount) {
        this.targetCurrencyAmount = targetCurrencyAmount;
    }
}
