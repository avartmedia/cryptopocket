package org.avartmedia.cryptopocket.backend.web.mvc;

import org.avartmedia.cryptopocket.backend.api.idm.IdmUser;
import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;
import org.avartmedia.cryptopocket.backend.api.idm.services.AuthenticationService;
import org.avartmedia.cryptopocket.backend.api.idm.services.IdmService;
import org.avartmedia.cryptopocket.backend.web.models.IdmUserRest;
import org.avartmedia.cryptopocket.backend.web.models.converter.IdmUserConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Christian on 13.12.2017.
 */
@Controller
@RequestMapping("/idm")
public class IdmController {

    private final IdmService idmService;
    private final AuthenticationService authenticationService;

    @Autowired
    public IdmController(IdmService idmService, AuthenticationService authenticationService) {
        this.idmService = idmService;
        this.authenticationService = authenticationService;
    }

    @Secured(value = Role.USER_CODE)
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<IdmUserRest> getAuthenticatedUser() {
        String username = authenticationService.getAuthenticatedUser().getUsername();
        IdmUser dto = idmService.findByUsername(username);

        IdmUserRest rest = IdmUserConverter.convertToRest(dto);
        return new ResponseEntity<>(rest, HttpStatus.OK);
    }
}
