package org.avartmedia.cryptopocket.backend.web.config;

import org.avartmedia.cryptopocket.backend.api.idm.services.IdmService;
import org.avartmedia.cryptopocket.backend.web.auth.AuthenticationTokenProcessingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final IdmService idmService;
    private final AuthenticationEntryPoint authenticationEntryPoint;
    private final AuthenticationTokenProcessingFilter processingFilter;

    @Autowired
    public SecurityConfig(IdmService idmService,
                          AuthenticationEntryPoint authenticationEntryPoint,
                          AuthenticationTokenProcessingFilter processingFilter) {
        this.idmService = idmService;
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.processingFilter = processingFilter;
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).and()

                .authorizeRequests()
                .antMatchers("/authentication/authenticate",
                        "/v2/api-docs",
                        "/swagger-resources/**",
                        "/**/**swagger-ui**/**",
                        "/favicon.ico")
                .permitAll()
                .anyRequest().authenticated().and()

                .csrf().disable()

                .addFilterBefore(processingFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authBuilder) throws Exception {
        authBuilder.userDetailsService(idmService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new StandardPasswordEncoder("ToTheMoon");
    }
}
