package org.avartmedia.cryptopocket.backend.web.models.converter;

import com.avartmedia.greencup.web.utils.RestTransformerUtils;
import org.avartmedia.cryptopocket.backend.api.idm.IdmUser;
import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;
import org.avartmedia.cryptopocket.backend.web.models.IdmUserRest;

import java.util.stream.Collectors;

public final class IdmUserConverter {

    public static IdmUserRest convertToRest(IdmUser dto) {
        if (dto == null) {
            return null;
        }

        IdmUserRest rest = new IdmUserRest();
        RestTransformerUtils.dtoToREST(dto, rest);

        rest.setUsername(dto.getUsername());
        rest.setRoles(dto.getRoles().stream().map(Role::getCode).collect(Collectors.toList()));

        // TODO Change when Userlocking is implemented
        rest.setEnabled(true);

        return rest;
    }
}
