package org.avartmedia.cryptopocket.backend.web.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Christian on 14.12.2017.
 */
public class UserWealthStatisticsRest implements Serializable {
    private static final long serialVersionUID = 6391975956483382311L;

    @JsonProperty("totalInvestment")
    private BigDecimal totalInvestment;

    @JsonProperty("totalSold")
    private BigDecimal totalSold;

    @JsonProperty("presentNetworth")
    private BigDecimal presentNetworth;

    public BigDecimal getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(BigDecimal totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public BigDecimal getTotalSold() {
        return totalSold;
    }

    public void setTotalSold(BigDecimal totalSold) {
        this.totalSold = totalSold;
    }

    public BigDecimal getPresentNetworth() {
        return presentNetworth;
    }

    public void setPresentNetworth(BigDecimal presentNetworth) {
        this.presentNetworth = presentNetworth;
    }
}
