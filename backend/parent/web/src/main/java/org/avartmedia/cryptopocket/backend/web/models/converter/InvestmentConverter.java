package org.avartmedia.cryptopocket.backend.web.models.converter;

import org.avartmedia.cryptopocket.backend.api.dto.InvestmentDTO;
import org.avartmedia.cryptopocket.backend.web.models.InvestmentRest;

/**
 * Created by Christian on 14.12.2017.
 */
public final class InvestmentConverter {

    public static InvestmentDTO convertToDTO(InvestmentRest rest) {
        if (rest == null) {
            return null;
        }

        InvestmentDTO dto = new InvestmentDTO();
        dto.setCurrencyCode(rest.getCurrencyCode());
        dto.setAmount(rest.getAmount());
        dto.setTotalPaid(rest.getInvestmentValue());
        dto.setInvestmentDate(rest.getPaymentDate());
        return dto;
    }
}
