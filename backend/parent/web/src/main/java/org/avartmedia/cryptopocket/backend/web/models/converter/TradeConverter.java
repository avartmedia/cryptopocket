package org.avartmedia.cryptopocket.backend.web.models.converter;

import org.avartmedia.cryptopocket.backend.api.dto.BalanceDTO;
import org.avartmedia.cryptopocket.backend.api.dto.TradeDTO;
import org.avartmedia.cryptopocket.backend.web.models.TradeRest;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by Christian on 14.12.2017.
 */
public final class TradeConverter {

    public static TradeDTO convertToDTO(TradeRest rest) {
        if (rest == null) {
            return null;
        }

        TradeDTO dto = new TradeDTO();
        dto.setTargetCurrencyCode(rest.getTargetCurrencyCode());
        dto.setTargetCurrencyAmount(rest.getTargetCurrencyAmount());

        HashMap<BalanceDTO, BigDecimal> sourceMap = new HashMap<>();
        dto.setSourceBalanceAmountMap(sourceMap);

        rest.getSourceList().forEach(r -> sourceMap.put(BalanceConverter.convertSingleRestToDTO(r.getBalance()), r.getAmount()));
        return dto;
    }
}
