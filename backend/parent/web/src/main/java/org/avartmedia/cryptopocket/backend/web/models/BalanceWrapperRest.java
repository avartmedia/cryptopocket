package org.avartmedia.cryptopocket.backend.web.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Christian on 14.12.2017.
 */
public class BalanceWrapperRest implements Serializable {
    private static final long serialVersionUID = -8357680206674460648L;

    @JsonProperty("balanceList")
    private List<BalanceRest> balanceList;

    public List<BalanceRest> getBalanceList() {
        return balanceList;
    }

    public void setBalanceList(List<BalanceRest> balanceList) {
        this.balanceList = balanceList;
    }
}
