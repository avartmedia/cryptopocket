package org.avartmedia.cryptopocket.backend.web.auth;

import com.avartmedia.greencup.api.auth.TokenWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.avartmedia.cryptopocket.backend.api.idm.services.AuthenticationService;
import org.avartmedia.cryptopocket.backend.core.config.ErrorCode;
import org.avartmedia.cryptopocket.backend.web.models.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component("authenticationTokenProcessingFilter")
public class AuthenticationTokenProcessingFilter extends GenericFilterBean {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationTokenProcessingFilter.class);

    private final AuthenticationService authenticationService;

    @Autowired
    public AuthenticationTokenProcessingFilter(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = getAsHttpRequest(servletRequest);
        HttpServletResponse httpResponse = getAsHttpResponse(servletResponse);

        String authenticationToken = extractAuthTokenFromRequest(httpRequest);
        WebAuthenticationDetails details = new WebAuthenticationDetailsSource().buildDetails(httpRequest);

        if (authenticationToken != null) {
            logger.debug("Starting Authentication-Check of Token {}", authenticationToken);

            try {
                authenticationService.validateToken(new TokenWrapper(authenticationToken), details);
                logger.debug("Authentication-Check for Token {} successfull.", authenticationToken);
            } catch (AuthenticationException e) {
                logger.debug("Authentication-Check for Token {} failed.", authenticationToken);

                ErrorMessage errorResponse = new ErrorMessage(HttpStatus.UNAUTHORIZED.value(), ErrorCode.UNAUTHORIZED,
                        "Authentication-Check for Token " + authenticationToken + " failed.");
                ResponseEntity<ErrorMessage> msg = new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);

                ObjectWriter writer = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String json = writer.writeValueAsString(msg.getBody());

                httpResponse.setContentType("application/json");
                httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                httpResponse.getOutputStream().println(json);
                httpResponse.getOutputStream().close();
            }
        }

        filterChain.doFilter(httpRequest, httpResponse);
    }


    private HttpServletRequest getAsHttpRequest(ServletRequest request) {
        if (!(request instanceof HttpServletRequest)) {
            throw new IllegalArgumentException("Expecting an HTTP-Request, but got: " + request);
        }
        return (HttpServletRequest) request;
    }

    private HttpServletResponse getAsHttpResponse(ServletResponse response) {
        if (!(response instanceof HttpServletResponse)) {
            throw new IllegalArgumentException("Expecting an HTTP-Response, but got: " + response);
        }
        return (HttpServletResponse) response;
    }

    private String extractAuthTokenFromRequest(HttpServletRequest httpRequest) {
        // Extract Token from header
        String authToken = httpRequest.getHeader("X-Auth-Token");

        // If Token not found: get it from request parameter
        if (authToken == null) {
            authToken = httpRequest.getParameter("token");
        }

        if (authToken == null) {
            logger.debug("No Authentication-Token found in Request {}", httpRequest);
        }

        return authToken;
    }
}
