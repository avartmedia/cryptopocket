package org.avartmedia.cryptopocket.backend.web.models.converter;

import org.avartmedia.cryptopocket.backend.api.dto.UserWealthStatisticsDTO;
import org.avartmedia.cryptopocket.backend.web.models.UserWealthStatisticsRest;

/**
 * Created by Christian on 14.12.2017.
 */
public final class UserWealthStatisticsConverter {

    public static UserWealthStatisticsRest convertToRest(UserWealthStatisticsDTO dto) {
        if (dto == null) {
            return null;
        }

        UserWealthStatisticsRest rest = new UserWealthStatisticsRest();
        rest.setPresentNetworth(dto.getPresentNetworth());
        rest.setTotalInvestment(dto.getTotalInvestment());
        rest.setTotalSold(dto.getTotalSold());
        return rest;
    }
}
