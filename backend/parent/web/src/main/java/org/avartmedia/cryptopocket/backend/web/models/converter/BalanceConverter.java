package org.avartmedia.cryptopocket.backend.web.models.converter;

import com.avartmedia.greencup.web.utils.RestTransformerUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.avartmedia.cryptopocket.backend.api.dto.BalanceDTO;
import org.avartmedia.cryptopocket.backend.web.models.BalanceRest;
import org.avartmedia.cryptopocket.backend.web.models.BalanceWrapperRest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christian on 14.12.2017.
 */
public final class BalanceConverter {

    public static BalanceWrapperRest convertToRest(List<BalanceDTO> dtoList) {
        BalanceWrapperRest rest = new BalanceWrapperRest();

        ArrayList<BalanceRest> restList = new ArrayList<>();
        rest.setBalanceList(restList);

        if (CollectionUtils.isEmpty(dtoList)) {
            return rest;
        }

        dtoList.forEach(dto -> restList.add(convertSingleDtoToRest(dto)));
        return rest;
    }

    public static BalanceRest convertSingleDtoToRest(BalanceDTO dto) {
        if (dto == null) {
            return null;
        }

        BalanceRest rest = new BalanceRest();
        RestTransformerUtils.dtoToREST(dto, rest);

        rest.setCurrencyCode(dto.getCurrencyCode());
        rest.setAmount(dto.getAmount());
        rest.setTotalPaymentPrice(dto.getTotalPaymentPrice());
        rest.setTotalPresentValue(dto.getTotalPresentValue());
        return rest;
    }

    public static BalanceDTO convertSingleRestToDTO(BalanceRest rest) {
        if (rest == null) {
            return null;
        }

        BalanceDTO dto = new BalanceDTO();
        RestTransformerUtils.restToDTO(rest, dto);

        dto.setCurrencyCode(rest.getCurrencyCode());
        dto.setAmount(rest.getAmount());
        dto.setTotalPaymentPrice(rest.getTotalPaymentPrice());
        dto.setTotalPresentValue(dto.getTotalPresentValue());
        return dto;
    }
}
