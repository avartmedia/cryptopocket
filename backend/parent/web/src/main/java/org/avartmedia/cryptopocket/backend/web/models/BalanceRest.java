package org.avartmedia.cryptopocket.backend.web.models;

import com.avartmedia.greencup.web.model.VersionedREST;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Created by Christian on 14.12.2017.
 */
public class BalanceRest extends VersionedREST<Long> {
    private static final long serialVersionUID = -5042872242113142359L;

    @JsonProperty("currencyCode")
    private String currencyCode;

    @JsonProperty("amount")
    private BigDecimal amount;

    @JsonProperty("totalPaymentPrice")
    private BigDecimal totalPaymentPrice;

    @JsonProperty("totalPresentValue")
    private BigDecimal totalPresentValue;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getTotalPaymentPrice() {
        return totalPaymentPrice;
    }

    public void setTotalPaymentPrice(BigDecimal totalPaymentPrice) {
        this.totalPaymentPrice = totalPaymentPrice;
    }

    public BigDecimal getTotalPresentValue() {
        return totalPresentValue;
    }

    public void setTotalPresentValue(BigDecimal totalPresentValue) {
        this.totalPresentValue = totalPresentValue;
    }
}
