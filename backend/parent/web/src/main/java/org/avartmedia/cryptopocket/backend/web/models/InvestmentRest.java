package org.avartmedia.cryptopocket.backend.web.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by Christian on 14.12.2017.
 */
public class InvestmentRest implements Serializable {
    private static final long serialVersionUID = -5296413791119043425L;

    @JsonProperty("currencyCode")
    private String currencyCode;

    @JsonProperty("amount")
    private BigDecimal amount;

    @JsonProperty("investmentValue")
    private BigDecimal investmentValue;

    @JsonProperty("paymentDate")
    private LocalDate paymentDate;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getInvestmentValue() {
        return investmentValue;
    }

    public void setInvestmentValue(BigDecimal investmentValue) {
        this.investmentValue = investmentValue;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }
}
