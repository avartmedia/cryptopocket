package org.avartmedia.cryptopocket.backend.web.utils;

import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;
import org.avartmedia.cryptopocket.backend.api.idm.services.AuthenticationService;
import org.avartmedia.cryptopocket.backend.api.idm.services.IdmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

/**
 * Created by Christian on 14.12.2017.
 */
@Component
public class IdmWebUtils {

    private final IdmService idmService;
    private final AuthenticationService authenticationService;

    @Autowired
    public IdmWebUtils(IdmService idmService, AuthenticationService authenticationService) {
        this.idmService = idmService;
        this.authenticationService = authenticationService;
    }

    @Secured(value = Role.USER_CODE)
    public Long getUserId() {
        String username = authenticationService.getAuthenticatedUser().getUsername();
        return idmService.getUserIdByUsername(username);
    }
}
