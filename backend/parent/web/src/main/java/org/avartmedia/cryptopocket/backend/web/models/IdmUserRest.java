package org.avartmedia.cryptopocket.backend.web.models;

import com.avartmedia.greencup.web.model.VersionedREST;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class IdmUserRest extends VersionedREST<Long> {
    private static final long serialVersionUID = 1716006274705847436L;

    @JsonProperty("username")
    private String username;

    @JsonProperty("roles")
    private List<String> roles;

    @JsonProperty("enabled")
    private boolean enabled;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
