package org.avartmedia.cryptopocket.backend.web.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Christian on 14.12.2017.
 */
public class TradeSourceRest implements Serializable {
    private static final long serialVersionUID = -8850138683264663898L;

    @JsonProperty("balance")
    private BalanceRest balance;

    @JsonProperty("amount")
    private BigDecimal amount;

    public BalanceRest getBalance() {
        return balance;
    }

    public void setBalance(BalanceRest balance) {
        this.balance = balance;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
