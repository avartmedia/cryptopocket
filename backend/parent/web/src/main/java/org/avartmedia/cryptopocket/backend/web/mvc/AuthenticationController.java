package org.avartmedia.cryptopocket.backend.web.mvc;

import com.avartmedia.greencup.api.auth.TokenWrapper;
import org.apache.commons.lang3.BooleanUtils;
import org.avartmedia.cryptopocket.backend.api.idm.IdmUser;
import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;
import org.avartmedia.cryptopocket.backend.api.idm.services.AuthenticationService;
import org.avartmedia.cryptopocket.backend.api.idm.services.IdmService;
import org.avartmedia.cryptopocket.backend.api.idm.token.TokenManager;
import org.avartmedia.cryptopocket.backend.web.models.CredentialsRest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/authentication")
public class AuthenticationController {

    private final IdmService idmService;
    private final AuthenticationService authenticationService;

    private final TokenManager tokenManager;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public AuthenticationController(IdmService idmService, AuthenticationService authenticationService,
                                    TokenManager tokenManager, AuthenticationManager authenticationManager) {
        this.idmService = idmService;
        this.authenticationService = authenticationService;
        this.tokenManager = tokenManager;
        this.authenticationManager = authenticationManager;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/authenticate")
    public ResponseEntity<TokenWrapper> authenticate(@RequestBody CredentialsRest credentials) {
        TokenWrapper tokenWrapper = authenticate(credentials.getUsername(), credentials.getPassword(),
                BooleanUtils.isTrue(credentials.getRememberToken()));

        return new ResponseEntity<>(tokenWrapper, HttpStatus.CREATED);
    }

    @Secured(value = Role.USER_CODE)
    @RequestMapping(method = RequestMethod.GET, value = "/verify")
    public ResponseEntity verify() {
        return new ResponseEntity(HttpStatus.OK);
    }

    private TokenWrapper authenticate(String username, String password, boolean rememberToken) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        IdmUser user = idmService.findByUsername(username);
        return tokenManager.createToken(user.getId(), rememberToken);
    }
}
