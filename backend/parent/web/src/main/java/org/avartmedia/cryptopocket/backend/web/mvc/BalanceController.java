package org.avartmedia.cryptopocket.backend.web.mvc;

import org.avartmedia.cryptopocket.backend.api.dto.BalanceDTO;
import org.avartmedia.cryptopocket.backend.api.dto.UserWealthStatisticsDTO;
import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;
import org.avartmedia.cryptopocket.backend.api.services.BalanceService;
import org.avartmedia.cryptopocket.backend.api.services.InvestmentService;
import org.avartmedia.cryptopocket.backend.web.models.BalanceWrapperRest;
import org.avartmedia.cryptopocket.backend.web.models.UserWealthStatisticsRest;
import org.avartmedia.cryptopocket.backend.web.models.converter.BalanceConverter;
import org.avartmedia.cryptopocket.backend.web.models.converter.UserWealthStatisticsConverter;
import org.avartmedia.cryptopocket.backend.web.utils.IdmWebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by Christian on 14.12.2017.
 */
@Controller
@RequestMapping("/balance")
public class BalanceController {

    private final IdmWebUtils idmWebUtils;
    private final BalanceService balanceService;
    private final InvestmentService investmentService;

    @Autowired
    public BalanceController(IdmWebUtils idmWebUtils, BalanceService balanceService, InvestmentService investmentService) {
        this.idmWebUtils = idmWebUtils;
        this.balanceService = balanceService;
        this.investmentService = investmentService;
    }

    @Secured(value = Role.USER_CODE)
    @RequestMapping(method = RequestMethod.GET, value = "/statistics")
    public ResponseEntity<UserWealthStatisticsRest> getStatistics() {
        UserWealthStatisticsDTO dto = investmentService.getTotalWealthByUser(idmWebUtils.getUserId());
        return new ResponseEntity<>(UserWealthStatisticsConverter.convertToRest(dto), HttpStatus.OK);
    }

    @Secured(value = Role.USER_CODE)
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<BalanceWrapperRest> findAllBalances() {
        List<BalanceDTO> dtoList = balanceService.findAllBalancesByUser(idmWebUtils.getUserId());
        return new ResponseEntity<>(BalanceConverter.convertToRest(dtoList), HttpStatus.OK);
    }
}
