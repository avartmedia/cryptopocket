package org.avartmedia.cryptopocket.backend.api.idm.services;

import org.avartmedia.cryptopocket.backend.api.idm.IdmUser;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface IdmService extends UserDetailsService {

    IdmUser findByUsername(String username);

    Long getUserIdByUsername(String username);
}
