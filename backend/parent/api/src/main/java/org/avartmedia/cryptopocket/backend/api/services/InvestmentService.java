package org.avartmedia.cryptopocket.backend.api.services;

import org.avartmedia.cryptopocket.backend.api.dto.InvestmentDTO;
import org.avartmedia.cryptopocket.backend.api.dto.UserWealthStatisticsDTO;

/**
 * Created by Christian on 14.12.2017.
 */
public interface InvestmentService {

    void createInvestment(Long userId, InvestmentDTO dto);

    void sellInvestment(Long userId, InvestmentDTO dto);

    UserWealthStatisticsDTO getTotalWealthByUser(Long userId);
}
