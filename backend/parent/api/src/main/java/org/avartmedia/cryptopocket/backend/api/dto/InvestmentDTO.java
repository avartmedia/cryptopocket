package org.avartmedia.cryptopocket.backend.api.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by Christian on 14.12.2017.
 */
public class InvestmentDTO {

    private String currencyCode;
    private BigDecimal amount;

    private BigDecimal totalPaid;
    private LocalDate investmentDate;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(BigDecimal totalPaid) {
        this.totalPaid = totalPaid;
    }

    public LocalDate getInvestmentDate() {
        return investmentDate;
    }

    public void setInvestmentDate(LocalDate investmentDate) {
        this.investmentDate = investmentDate;
    }
}
