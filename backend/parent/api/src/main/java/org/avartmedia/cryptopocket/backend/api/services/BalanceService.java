package org.avartmedia.cryptopocket.backend.api.services;

import com.avartmedia.greencup.api.service.GenericReadService;
import org.avartmedia.cryptopocket.backend.api.dto.BalanceDTO;

import java.util.List;

/**
 * Created by Christian on 14.12.2017.
 */
public interface BalanceService extends GenericReadService<Long, BalanceDTO> {

    List<BalanceDTO> findAllBalancesByUser(Long userId);
}
