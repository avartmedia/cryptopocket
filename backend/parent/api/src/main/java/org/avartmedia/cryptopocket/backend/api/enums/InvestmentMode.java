package org.avartmedia.cryptopocket.backend.api.enums;

import com.avartmedia.greencup.api.enums.ServiceEnum;
import com.avartmedia.greencup.utils.enums.ServiceEnumUtils;

/**
 * Created by Christian on 15.12.2017.
 */
public enum InvestmentMode implements ServiceEnum<String> {
    BUY("INVBUY"),
    SELL("INVSELL");

    private final String code;

    InvestmentMode(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

    public static InvestmentMode getByCode(String code) {
        return ServiceEnumUtils.getByCode(code, InvestmentMode.class);
    }
}
