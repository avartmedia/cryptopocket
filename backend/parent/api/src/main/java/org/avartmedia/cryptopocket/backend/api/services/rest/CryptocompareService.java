package org.avartmedia.cryptopocket.backend.api.services.rest;

import java.math.BigDecimal;

/**
 * Wrapper for <a href="https://www.cryptocompare.com/api/#-api-data-coinlist-">Cryptocompare-API</a>
 */
public interface CryptocompareService {

    BigDecimal getCurrencyValue(String currencyCode, BigDecimal amount);
}
