package org.avartmedia.cryptopocket.backend.api.dto;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by Christian on 14.12.2017.
 */
public class TradeDTO {

    /**
     * Map between {@link BalanceDTO} and the amount of the Key's Currency used.
     */
    private Map<BalanceDTO, BigDecimal> sourceBalanceAmountMap;

    private String targetCurrencyCode;
    private BigDecimal targetCurrencyAmount;

    public Map<BalanceDTO, BigDecimal> getSourceBalanceAmountMap() {
        return sourceBalanceAmountMap;
    }

    public void setSourceBalanceAmountMap(Map<BalanceDTO, BigDecimal> sourceBalanceAmountMap) {
        this.sourceBalanceAmountMap = sourceBalanceAmountMap;
    }

    public String getTargetCurrencyCode() {
        return targetCurrencyCode;
    }

    public void setTargetCurrencyCode(String targetCurrencyCode) {
        this.targetCurrencyCode = targetCurrencyCode;
    }

    public BigDecimal getTargetCurrencyAmount() {
        return targetCurrencyAmount;
    }

    public void setTargetCurrencyAmount(BigDecimal targetCurrencyAmount) {
        this.targetCurrencyAmount = targetCurrencyAmount;
    }
}
