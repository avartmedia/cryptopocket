package org.avartmedia.cryptopocket.backend.api.idm.token;

import com.avartmedia.greencup.api.auth.TokenWrapper;

import java.util.List;

public interface TokenManager {

    TokenWrapper createToken(Long userId, boolean persisted);

    void removeByUserId(Long userId);

    void removeToken(Long userId, String token);

    List<String> getTokensByUserId(Long userId);

    String extractUsernameFromToken(TokenWrapper token);

    boolean isTokenValid(Long userId, TokenWrapper token);
}
