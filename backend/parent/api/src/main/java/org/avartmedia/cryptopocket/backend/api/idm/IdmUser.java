package org.avartmedia.cryptopocket.backend.api.idm;

import com.avartmedia.greencup.api.dto.VersionedDTO;
import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;

import java.util.List;

public class IdmUser extends VersionedDTO<Long> {

    private String username;
    private String password;

    private List<Role> roles;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
