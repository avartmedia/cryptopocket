package org.avartmedia.cryptopocket.backend.api.idm.services;

import com.avartmedia.greencup.api.auth.TokenWrapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

public interface AuthenticationService {

    void validateToken(TokenWrapper token, WebAuthenticationDetails details) throws AuthenticationException;

    UserDetails getAuthenticatedUser();
}
