package org.avartmedia.cryptopocket.backend.api.idm.enums;

import com.avartmedia.greencup.api.enums.ServiceEnum;
import com.avartmedia.greencup.utils.enums.ServiceEnumUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum Role implements ServiceEnum<String> {
    ADMIN("ADMIN"),
    USER("USER");

    public static final String ROLE_PREFIX = "ROLE_";

    public static final String ADMIN_CODE = "ROLE_ADMIN";
    public static final String USER_CODE = "ROLE_USER";

    private final String code;

    Role(final String code) {
        this.code = code;
    }

    public static Role getByCode(final String code) {
        return ServiceEnumUtils.getByCode(code, Role.class);
    }

    public static List<Role> getRolesByCode(final String code) {
        Role userRole = getByCode(code);

        if (userRole == null) {
            return Collections.emptyList();
        }

        List<Role> roleList = new ArrayList<>();
        roleList.add(userRole);

        if (userRole == ADMIN) {
            roleList.add(USER);
        }

        return Collections.unmodifiableList(roleList);
    }

    @Override
    public String getCode() {
        return code;
    }
}
