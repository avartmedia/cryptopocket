package org.avartmedia.cryptopocket.backend.api.dto.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Christian on 16.12.2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CryptocompareCoinlistEntryResponse implements Serializable {
    private static final long serialVersionUID = 8945588046317482043L;

    @JsonProperty("Id")
    private Long id;

    @JsonProperty("Url")
    private String url;

    @JsonProperty("ImageUrl")
    private String imageUrl;

    @JsonProperty("Name")
    private String code;

    @JsonProperty("Symbol")
    private String symbol;

    @JsonProperty("CoinName")
    private String name;

    @JsonProperty("FullName")
    private String fullName;

    public Long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getCode() {
        return code;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }
}
