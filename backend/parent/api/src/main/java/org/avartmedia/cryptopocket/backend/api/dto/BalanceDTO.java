package org.avartmedia.cryptopocket.backend.api.dto;

import com.avartmedia.greencup.api.dto.VersionedDTO;

import java.math.BigDecimal;

/**
 * Balance-Information of a single Crypto-Currency.
 */
public class BalanceDTO extends VersionedDTO<Long> {

    /**
     * Short Code of the Crypto-Currency.
     */
    private String currencyCode;

    /**
     * Total amount of the Crypto-Currency.
     */
    private BigDecimal amount;

    /**
     * Total price paid for the Currency in Dollar.
     * Contains Investments and Trades.
     */
    private BigDecimal totalPaymentPrice;

    /**
     * Not persisted.
     * Newly calculated on every Read-Call.
     */
    private BigDecimal totalPresentValue;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getTotalPaymentPrice() {
        return totalPaymentPrice;
    }

    public void setTotalPaymentPrice(BigDecimal totalPaymentPrice) {
        this.totalPaymentPrice = totalPaymentPrice;
    }

    public BigDecimal getTotalPresentValue() {
        return totalPresentValue;
    }

    public void setTotalPresentValue(BigDecimal totalPresentValue) {
        this.totalPresentValue = totalPresentValue;
    }
}
