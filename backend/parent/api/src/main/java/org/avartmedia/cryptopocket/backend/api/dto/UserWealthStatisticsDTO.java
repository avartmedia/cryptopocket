package org.avartmedia.cryptopocket.backend.api.dto;

import java.math.BigDecimal;

/**
 * Wrapps all basic Informations for the Users Balance.
 */
public class UserWealthStatisticsDTO {

    /**
     * Summary of all paid Investments in Dollar.
     * Does not contain Trades.
     */
    private final BigDecimal totalInvestment;

    /**
     * Total of all sold Crypto-Currencies in Dollar.
     * Does not contain Trades.
     */
    private final BigDecimal totalSold;

    /**
     * Summary of the total Value of all Balances.
     */
    private final BigDecimal presentNetworth;

    public UserWealthStatisticsDTO(BigDecimal totalInvestment, BigDecimal totalSold, BigDecimal presentNetworth) {
        this.totalInvestment = totalInvestment;
        this.totalSold = totalSold;
        this.presentNetworth = presentNetworth;
    }

    public BigDecimal getTotalInvestment() {
        return totalInvestment;
    }

    public BigDecimal getTotalSold() {
        return totalSold;
    }

    public BigDecimal getPresentNetworth() {
        return presentNetworth;
    }
}
