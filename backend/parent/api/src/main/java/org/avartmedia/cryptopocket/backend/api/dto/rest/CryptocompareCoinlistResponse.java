package org.avartmedia.cryptopocket.backend.api.dto.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Christian on 16.12.2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CryptocompareCoinlistResponse implements Serializable {
    private static final long serialVersionUID = 5854476376022544946L;

    @JsonProperty("Response")
    private String status;

    @JsonProperty("Data")
    private Map<String, CryptocompareCoinlistEntryResponse> coinlist;

    public String getStatus() {
        return status;
    }

    public Map<String, CryptocompareCoinlistEntryResponse> getCoinlist() {
        return coinlist;
    }
}
