package org.avartmedia.cryptopocket.backend.api.idm.exceptions;

import com.avartmedia.greencup.api.exception.InternalServiceException;

public class NoUserAuthenticatedException extends InternalServiceException {

    public NoUserAuthenticatedException() {
        super("No User authenticated");
    }
}
