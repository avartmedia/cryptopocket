package org.avartmedia.cryptopocket.backend.api.services;

import org.avartmedia.cryptopocket.backend.api.dto.TradeDTO;

/**
 * Created by Christian on 14.12.2017.
 */
public interface TradeService {

    void tradeBalance(Long userId, TradeDTO trade);

    // TODO History
}
