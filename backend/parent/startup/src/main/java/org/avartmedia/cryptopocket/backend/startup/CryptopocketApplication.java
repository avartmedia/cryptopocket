package org.avartmedia.cryptopocket.backend.startup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {
        CryptopocketApplication.WEB_BASE_PACKAGE,
        CryptopocketApplication.BACKEND_BASE_PACKAGE,
        CryptopocketApplication.STARTUP_BASE_PACKAGE,
})
@EntityScan(basePackages = CryptopocketApplication.ENTITY_BASE_PACKAGE)
@EnableJpaRepositories(basePackages = CryptopocketApplication.REPOSITORY_BASE_PACKAGE)
public class CryptopocketApplication {

    public static final String WEB_BASE_PACKAGE = "org.avartmedia.cryptopocket.backend.web";
    public static final String BACKEND_BASE_PACKAGE = "org.avartmedia.cryptopocket.backend.be";
    public static final String STARTUP_BASE_PACKAGE = "org.avartmedia.cryptopocket.backend.startup";

    public static final String ENTITY_BASE_PACKAGE = BACKEND_BASE_PACKAGE + ".entities";
    public static final String REPOSITORY_BASE_PACKAGE = BACKEND_BASE_PACKAGE + ".repositories";

    public static void main(String args[]) throws Throwable {
        SpringApplication.run(CryptopocketApplication.class, args);
    }
}
