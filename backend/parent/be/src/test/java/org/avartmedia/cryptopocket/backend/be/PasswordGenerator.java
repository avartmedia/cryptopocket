package org.avartmedia.cryptopocket.backend.be;

import org.junit.Test;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

public class PasswordGenerator {

    @Test
    public void generate() {
        String password = "admin";

        String encoded = new StandardPasswordEncoder("ToTheMoon").encode(password);
        System.out.println("Encoded Password: " + encoded);
    }
}
