package org.avartmedia.cryptopocket.backend.be.entities;

import com.avartmedia.greencup.be.entity.VersionedEntity;
import com.avartmedia.greencup.be.utils.EntityColumnLength;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Christian on 15.12.2017.
 */
@Entity
@Table(name = "INVESTMENT")
public class InvestmentEntity extends VersionedEntity<Long> {

    @Id
    @Column(name = "INVESTMENT_PK", unique = true, updatable = false, nullable = false)
    @SequenceGenerator(name = "INVESTMENT_SQ", sequenceName = "INVESTMENT_SQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INVESTMENT_SQ")
    private Long id;

    /**
     * @see org.avartmedia.cryptopocket.backend.api.enums.InvestmentMode
     */
    @Column(name = "MODE", length = EntityColumnLength.CODE, updatable = false, nullable = false)
    private String mode;

    @Column(name = "CURRENCY_CODE", length = EntityColumnLength.CODE, updatable = false, nullable = false)
    private String currencyCode;

    @Column(name = "AMOUNT", nullable = false)
    private Double amount;

    @Column(name = "TOTAL_PRICE", updatable = false, nullable = false)
    private Double totalPrice;

    @Temporal(TemporalType.DATE)
    @Column(name = "INVESTMENT_DATE", updatable = false, nullable = false)
    private Date investmentDate;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "USER_FK", updatable = false)
    private User user;

    @Override
    public Long getId() {
        return id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getInvestmentDate() {
        return investmentDate;
    }

    public void setInvestmentDate(Date investmentDate) {
        this.investmentDate = investmentDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
