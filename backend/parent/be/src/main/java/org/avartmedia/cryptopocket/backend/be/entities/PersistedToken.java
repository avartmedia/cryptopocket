package org.avartmedia.cryptopocket.backend.be.entities;

import com.avartmedia.greencup.be.entity.VersionedEntity;

import javax.persistence.*;

@Entity
@Table(name = "TOKEN")
public class PersistedToken extends VersionedEntity<Long> {

    @Id
    @Column(name = "TOKEN_PK", nullable = false, updatable = false, unique = true)
    @SequenceGenerator(name = "TOKEN_SQ", sequenceName = "TOKEN_SQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOKEN_SQ")
    private Long id;

    @Column(name = "TOKEN_VALUE", unique = true, updatable = false, nullable = false)
    private String token;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "USER_FK")
    private User user;

    @Override
    public Long getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
