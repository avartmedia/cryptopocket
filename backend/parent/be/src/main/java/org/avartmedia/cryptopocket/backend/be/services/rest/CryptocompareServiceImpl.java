package org.avartmedia.cryptopocket.backend.be.services.rest;

import com.avartmedia.greencup.api.exception.InternalServiceException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.avartmedia.cryptopocket.backend.api.dto.rest.CryptocompareCoinlistEntryResponse;
import org.avartmedia.cryptopocket.backend.api.dto.rest.CryptocompareCoinlistResponse;
import org.avartmedia.cryptopocket.backend.api.services.rest.CryptocompareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of {@link CryptocompareService}
 */
@Service
public class CryptocompareServiceImpl implements CryptocompareService {

    private static final Integer CACHE_VALIDITY = 60;

    // Lazy-Loading, Access via #getCoinMap
    private static Map<String, CryptocompareCoinlistEntryResponse> _coinMap;
    private static LocalDate _coinMapUpdated;

    // Lazy-Loading, Access via #getCoinMap
    private static Map<String, BigDecimal> _currencyPriceMap = new HashMap<>();
    private static LocalDateTime _currencyPriceMapUpdated;

    private final RestTemplate rest;
    private final HttpHeaders headers;
    private final ObjectMapper mapper;

    private @Value("${api.cryptocompare.base}") String apiBase;

    private @Value("${api.cryptocompare.coinlist}") String apiCoinlist;
    private @Value("${api.cryptocompare.price}") String pricePerCoin;


    @Autowired
    public CryptocompareServiceImpl() {
        rest = new RestTemplate();

        headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    @Override
    public BigDecimal getCurrencyValue(String currencyCode, BigDecimal amount) {
        Map<String, CryptocompareCoinlistEntryResponse> coinMap = getCoinMap();

        if (!coinMap.containsKey(currencyCode)) { // TODO Show User
            throw new InternalServiceException("No Coinlist-Entry for Currency-Code " + currencyCode + " found.");
        }

        if (_currencyPriceMap.containsKey(currencyCode) && _currencyPriceMapUpdated.plusSeconds(CACHE_VALIDITY).isAfter(LocalDateTime.now())) {
            return _currencyPriceMap.get(currencyCode).multiply(amount);
        }

        String targetCurrency = "USD";
        String symbol = coinMap.get(currencyCode).getSymbol();

        String url = apiBase + pricePerCoin.replace("{0}", symbol).replace("{1}", targetCurrency);
        ResponseEntity<String> response = rest.exchange(url, HttpMethod.GET, new HttpEntity(headers), String.class);

        BigDecimal currencyValue;
        try {
            Map<String, Double> map = mapper.readValue(response.getBody(), Map.class);
            currencyValue = BigDecimal.valueOf(map.get(targetCurrency));

            _currencyPriceMapUpdated = LocalDateTime.now();
            _currencyPriceMap.put(currencyCode, currencyValue);
        } catch (IOException e) {
            throw new InternalServiceException("Error mapping Response.", e);
        }

        return currencyValue.multiply(amount);
    }

    private Map<String, CryptocompareCoinlistEntryResponse> getCoinMap() {
        if (_coinMap == null || _coinMapUpdated.isBefore(LocalDate.now())) {
            try {
                ResponseEntity<String> response = rest.exchange(apiBase + apiCoinlist, HttpMethod.GET, new HttpEntity(headers), String.class);
                CryptocompareCoinlistResponse ccResponse = mapper.readValue(response.getBody(), CryptocompareCoinlistResponse.class);

                _coinMapUpdated = LocalDate.now();
                _coinMap = ccResponse.getCoinlist();
            } catch (IOException e) {
                throw new InternalServiceException("Error mapping Response.", e);
            }
        }

        return _coinMap;
    }


    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyLoader() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        configurer.setLocations(new ClassPathResource("/org/avartmedia/cryptopocket/backend/be/app-backend.properties"));
        return configurer;
    }
}
