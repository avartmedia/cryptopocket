package org.avartmedia.cryptopocket.backend.be.idm.services.mock;

import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;
import org.avartmedia.cryptopocket.backend.api.idm.services.IdmService;
import org.avartmedia.cryptopocket.backend.be.entities.User;
import org.avartmedia.cryptopocket.backend.be.idm.services.IdmServiceImpl;
import org.avartmedia.cryptopocket.backend.be.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by Christian on 15.12.2017.
 */
@Service
@Primary
@Profile("mock-authentication")
public class MockIdmService extends IdmServiceImpl implements IdmService {

    private static final Logger logger = LoggerFactory.getLogger(MockIdmService.class);

    private final UserRepository repository;

    @Autowired
    public MockIdmService(UserRepository repository) {
        super(repository);
        logger.warn("Mock 'mock-authentication' active.");

        this.repository = repository;

        init();
    }

    private void init() {
        persistUser("user1", Role.USER);
        persistUser("user2", Role.USER);
        persistUser("user3", Role.USER);
        persistUser("admin1", Role.ADMIN);
        persistUser("admin2", Role.ADMIN);
    }

    private void persistUser(String username, Role role) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(new StandardPasswordEncoder("ToTheMoon").encode(username));
        user.setRole(role.getCode());

        repository.save(user);
    }
}
