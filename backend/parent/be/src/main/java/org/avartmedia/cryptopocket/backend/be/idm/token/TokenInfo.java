package org.avartmedia.cryptopocket.backend.be.idm.token;

import java.time.LocalDateTime;

public class TokenInfo {

    private String token;
    private Long userId;

    // 'null' if Token is persisted and lasts until removed by User
    private LocalDateTime expiresAt;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDateTime getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(LocalDateTime expiresAt) {
        this.expiresAt = expiresAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TokenInfo tokenInfo = (TokenInfo) o;

        if (!token.equals(tokenInfo.token)) return false;
        if (!userId.equals(tokenInfo.userId)) return false;
        return expiresAt.equals(tokenInfo.expiresAt);
    }

    @Override
    public int hashCode() {
        int result = token.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + expiresAt.hashCode();
        return result;
    }
}
