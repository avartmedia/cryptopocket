package org.avartmedia.cryptopocket.backend.be.idm.services;

import com.avartmedia.greencup.api.exception.EntityNotFoundException;
import org.apache.commons.lang3.Validate;
import org.avartmedia.cryptopocket.backend.api.idm.IdmUser;
import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;
import org.avartmedia.cryptopocket.backend.api.idm.services.IdmService;
import org.avartmedia.cryptopocket.backend.be.entities.User;
import org.avartmedia.cryptopocket.backend.be.idm.UserDetailsImpl;
import org.avartmedia.cryptopocket.backend.be.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class IdmServiceImpl implements IdmService {

    private final UserRepository userRepository;

    @Autowired
    public IdmServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public IdmUser findByUsername(String username) {
        Validate.notNull(username, "Username must be set.");

        User entity = findEntityByUsername(username);

        IdmUser user = new IdmUser();
        user.setId(entity.getId());
        user.setVersion(entity.getVersion());

        user.setUsername(entity.getUsername());
        user.setPassword(entity.getPassword());
        user.setRoles(Role.getRolesByCode(entity.getRole()));
        return user;
    }

    @Override
    public Long getUserIdByUsername(String username) {
        Validate.notNull(username, "Username must be set.");

        User entity = findEntityByUsername(username);
        return entity.getId();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            Validate.notNull(username, "Username must be set.");

            IdmUser idmUser = findByUsername(username);
            return new UserDetailsImpl(idmUser);
        } catch (Exception e) {
            throw new UsernameNotFoundException("No User for Username " + username + " found.", e);
        }
    }

    private User findEntityByUsername(String username) {
        User entity = userRepository.findByUsername(username);
        if (entity == null) {
            throw new EntityNotFoundException(User.class, username);
        }

        return entity;
    }
}
