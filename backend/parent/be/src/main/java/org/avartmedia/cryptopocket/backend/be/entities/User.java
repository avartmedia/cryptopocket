package org.avartmedia.cryptopocket.backend.be.entities;

import com.avartmedia.greencup.be.entity.VersionedEntity;
import com.avartmedia.greencup.be.utils.EntityColumnLength;

import javax.persistence.*;

@Entity
@Table(name = "USER")
public class User extends VersionedEntity<Long> {

    @Id
    @Column(name = "USER_PK", unique = true, updatable = false, nullable = false)
    @SequenceGenerator(name = "USER_SQ", sequenceName = "USER_SQ", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SQ")
    private Long id;

    @Column(name = "USERNAME", length = EntityColumnLength.USERNAME, nullable = false)
    private String username;

    @Column(name = "PASSWORD", length = EntityColumnLength.PASSWORD, nullable = false)
    private String password;

    @Column(name = "ROLE", length = EntityColumnLength.CODE, nullable = false)
    private String role;

    @Override
    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
