package org.avartmedia.cryptopocket.backend.be.idm.token;

import com.avartmedia.greencup.api.auth.TokenWrapper;
import com.avartmedia.greencup.api.exception.InternalServiceException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.Validate;
import org.avartmedia.cryptopocket.backend.api.idm.token.TokenManager;
import org.avartmedia.cryptopocket.backend.be.entities.PersistedToken;
import org.avartmedia.cryptopocket.backend.be.entities.User;
import org.avartmedia.cryptopocket.backend.be.repositories.PersistedTokenRepository;
import org.avartmedia.cryptopocket.backend.be.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of {@link TokenManager}.
 * Supports multiple Tokens for a single User.
 */
@Service
public class MultiTokenManagerImpl implements TokenManager {

    private static final Logger logger = LoggerFactory.getLogger(MultiTokenManagerImpl.class);

    private static final Integer MINUTES_VALID = 60;

    private final UserRepository userRepository;
    private final PersistedTokenRepository tokenRepository;

    private final Map<Long, List<TokenInfo>> tokenMap;

    @Autowired
    public MultiTokenManagerImpl(UserRepository userRepository, PersistedTokenRepository tokenRepository) {
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;

        tokenMap = new HashMap<>();

        for (PersistedToken token : tokenRepository.findAll()) {
            Long userId = token.getUser().getId();

            TokenInfo tokenInfo = new TokenInfo();
            tokenInfo.setToken(token.getToken());
            tokenInfo.setUserId(userId);

            List<TokenInfo> tokenInfosForUser = tokenMap.computeIfAbsent(userId, id -> new ArrayList<>());
            tokenInfosForUser.add(tokenInfo);
        }
    }

    @Override
    public TokenWrapper createToken(Long userId, boolean persisted) {
        Validate.notNull(userId, "UserId must be set.");

        User user = userRepository.getOne(userId);

        List<TokenInfo> tokensForUser = tokenMap.computeIfAbsent(userId, id -> new ArrayList<>());

        TokenInfo tokenInfo = new TokenInfo();
        tokenInfo.setUserId(userId);

        LocalDateTime expiresAt = persisted ? null : LocalDateTime.now().plusMinutes(MINUTES_VALID);
        tokenInfo.setExpiresAt(expiresAt);

        TokenWrapper generatedToken;
        do {
            generatedToken = generateToken(user.getUsername(), expiresAt);
            tokenInfo.setToken(generatedToken.getToken());
        } while (tokensForUser.contains(tokenInfo));

        tokensForUser.add(tokenInfo);

        if (persisted) {
            PersistedToken persistedToken = new PersistedToken();
            persistedToken.setToken(tokenInfo.getToken());
            persistedToken.setUser(user);
            tokenRepository.save(persistedToken);
        }

        return generatedToken;
    }

    @Override
    public void removeByUserId(Long userId) {
        Validate.notNull(userId, "UserId must be set.");

        List<TokenInfo> previousValue = tokenMap.remove(userId);

        if (CollectionUtils.isEmpty(previousValue)) {
            logger.warn("No Tokens for UserId {} found.", userId);
            return;
        }

        previousValue.stream()
                .filter(token -> token.getExpiresAt() != null)
                .map(TokenInfo::getToken)
                .forEach(tokenRepository::deleteByToken);
    }

    @Override
    public void removeToken(Long userId, String token) {
        Validate.notNull(userId, "UserId must be set.");
        Validate.notEmpty(token, "Token must be set.");

        List<TokenInfo> tokenInfos = tokenMap.get(userId);
        Validate.notEmpty(tokenInfos, "No Tokens for UserId " + userId + " found.");

        TokenInfo filteredToken = tokenInfos.stream()
                .filter(filtered -> filtered.getToken().equals(token))
                .findFirst()
                .orElse(null);
        Validate.notNull(filteredToken, "No Result in Map for Token " + token + " found.");

        tokenInfos.remove(filteredToken);

        if (filteredToken.getExpiresAt() != null) {
            tokenRepository.deleteByToken(filteredToken.getToken());
        }
    }

    @Override
    public List<String> getTokensByUserId(Long userId) {
        Validate.notNull(userId, "UserId must be set.");

        List<TokenInfo> tokenInfos = tokenMap.get(userId);

        if (CollectionUtils.isEmpty(tokenInfos)) {
            logger.warn("No Tokens for User {} found.", userId);
            return Collections.emptyList();
        }

        return tokenInfos.stream()
                .map(TokenInfo::getToken)
                .collect(Collectors.toList());
    }

    @Override
    public String extractUsernameFromToken(TokenWrapper token) {
        Validate.notNull(token, "Token must be set.");

        String[] split = token.getToken().split(":");
        if (split.length != 2 && split.length != 3) {
            throw new InternalServiceException("Invalid amount of ':' in Token " + token.getToken());
        }

        return split[1];
    }

    @Override
    public boolean isTokenValid(Long userId, TokenWrapper token) {
        Validate.notNull(userId, "UserId must be set.");
        Validate.notNull(token, "Token must be set.");

        List<TokenInfo> tokenInfos = tokenMap.get(userId);
        if (CollectionUtils.isEmpty(tokenInfos)) {
            logger.warn("No Tokens for User {} found. Validity-Check failed.", userId);
            return false;
        }

        return tokenInfos.stream()
                .filter(ti -> ti.getExpiresAt() == null || ti.getExpiresAt().isAfter(LocalDateTime.now()))
                .map(TokenInfo::getToken)
                .collect(Collectors.toList())
                .contains(token.getToken());
    }


    private TokenWrapper generateToken(String username, LocalDateTime expireDate) {
        byte[] tokenBytes = new byte[32];
        new SecureRandom().nextBytes(tokenBytes);

        StringBuilder builder = new StringBuilder(new String(Base64.encode(tokenBytes), StandardCharsets.UTF_8));

        // Append Username
        builder.append(":").append(username);

        // Append Expire-Date
        if (expireDate != null) {
            builder.append(":").append(expireDate.toInstant(ZoneOffset.UTC).toEpochMilli());
        }

        return new TokenWrapper(builder.toString());
    }
}
