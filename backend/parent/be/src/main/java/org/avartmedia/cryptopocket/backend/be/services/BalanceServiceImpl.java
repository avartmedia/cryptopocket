package org.avartmedia.cryptopocket.backend.be.services;

import com.avartmedia.greencup.be.service.GenericCrudServiceImpl;
import com.avartmedia.greencup.be.utils.Transformer;
import org.apache.commons.lang3.Validate;
import org.avartmedia.cryptopocket.backend.api.dto.BalanceDTO;
import org.avartmedia.cryptopocket.backend.api.services.BalanceService;
import org.avartmedia.cryptopocket.backend.api.services.rest.CryptocompareService;
import org.avartmedia.cryptopocket.backend.be.entities.BalanceEntity;
import org.avartmedia.cryptopocket.backend.be.repositories.BalanceRepository;
import org.avartmedia.cryptopocket.backend.be.repositories.UserRepository;
import org.avartmedia.cryptopocket.backend.be.utils.transformer.BalanceTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christian on 14.12.2017.
 */
@Service
public class BalanceServiceImpl extends GenericCrudServiceImpl<Long, BalanceDTO, BalanceEntity> implements BalanceService {

    private static final Logger logger = LoggerFactory.getLogger(BalanceServiceImpl.class);

    private final BalanceRepository repository;
    private final UserRepository userRepository;

    private final CryptocompareService cryptocompareService;

    // Lazy-Loading, access via #getTransformer
    private BalanceTransformer _transformer;

    @Autowired
    public BalanceServiceImpl(BalanceRepository repository, UserRepository userRepository, CryptocompareService cryptocompareService) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.cryptocompareService = cryptocompareService;
    }

    @Override
    public List<BalanceDTO> findAllBalancesByUser(Long userId) {
        Validate.notNull(userId, "UserId must be set.");

        List<BalanceEntity> entityList = repository.findByUserId(userId);

        List<BalanceDTO> dtoList = new ArrayList<>();
        entityList.forEach(e -> dtoList.add(getTransformer().transformInverse(e)));
        dtoList.forEach(d -> d.setTotalPresentValue(cryptocompareService.getCurrencyValue(d.getCurrencyCode(), d.getAmount())));
        return dtoList;
    }

    @Override
    public BalanceDTO read(Long id) {
        BalanceDTO dto = super.read(id);
        dto.setTotalPresentValue(cryptocompareService.getCurrencyValue(dto.getCurrencyCode(), dto.getAmount()));
        return dto;
    }

    @Override
    protected Transformer<BalanceDTO, BalanceEntity> getTransformer() {
        if (_transformer == null) {
            _transformer = new BalanceTransformer();
        }
        return _transformer;
    }

    @Override
    protected Class<BalanceEntity> getEntityClazz() {
        return BalanceEntity.class;
    }

    @Override
    protected JpaRepository<BalanceEntity, Long> getRepository() {
        return repository;
    }

    private void validate(BalanceDTO dto) {
        Validate.notNull(dto, "Dto must be set.");
        Validate.notNull(dto.getCurrencyCode(), "CurrencyCode must be set.");
        Validate.notNull(dto.getAmount(), "Amount must be set.");
        Validate.notNull(dto.getTotalPaymentPrice(), "TotalPaymentPrice must be set.");
    }
}
