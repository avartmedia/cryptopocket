package org.avartmedia.cryptopocket.backend.be.repositories;

import org.avartmedia.cryptopocket.backend.be.entities.PersistedToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersistedTokenRepository extends JpaRepository<PersistedToken, Long> {

    void deleteByToken(String token);
}
