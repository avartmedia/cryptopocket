package org.avartmedia.cryptopocket.backend.be.utils.transformer;

import com.avartmedia.greencup.be.utils.Transformer;
import org.apache.commons.lang3.Validate;
import org.avartmedia.cryptopocket.backend.api.dto.BalanceDTO;
import org.avartmedia.cryptopocket.backend.be.entities.BalanceEntity;

import java.math.BigDecimal;

/**
 * Created by Christian on 14.12.2017.
 */
public class BalanceTransformer implements Transformer<BalanceDTO, BalanceEntity> {

    @Override
    public BalanceEntity transform(BalanceDTO source) {
        return transform(source, new BalanceEntity());
    }

    @Override
    public BalanceEntity transform(BalanceDTO source, BalanceEntity entity) {
        Validate.notNull(source, "Source must be set.");
        Validate.notNull(entity, "Entity must be set.");

        entity.setCurrencyCode(source.getCurrencyCode());
        entity.setAmount(source.getAmount().doubleValue());
        entity.setTotalPaymentPrice(source.getTotalPaymentPrice().doubleValue());
        return entity;
    }

    @Override
    public BalanceDTO transformInverse(BalanceEntity source) {
        return transformInverse(source, new BalanceDTO());
    }

    @Override
    public BalanceDTO transformInverse(BalanceEntity source, BalanceDTO dto) {
        Validate.notNull(source, "Source must be set.");
        Validate.notNull(dto, "Dto must be set.");

        dto.setCurrencyCode(source.getCurrencyCode());
        dto.setAmount(BigDecimal.valueOf(source.getAmount()));
        dto.setTotalPaymentPrice(BigDecimal.valueOf(source.getTotalPaymentPrice()));
        return dto;
    }
}
