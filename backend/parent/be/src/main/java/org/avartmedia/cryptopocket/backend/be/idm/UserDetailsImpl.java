package org.avartmedia.cryptopocket.backend.be.idm;

import org.apache.commons.lang3.Validate;
import org.avartmedia.cryptopocket.backend.api.idm.IdmUser;
import org.avartmedia.cryptopocket.backend.api.idm.enums.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class UserDetailsImpl implements UserDetails {

    private final String username;
    private final String password;

    private final Set<GrantedAuthority> authorities;
    private final boolean enabled;

    public UserDetailsImpl(IdmUser user) {
        Validate.notNull(user, "User must be set.");

        username = user.getUsername();
        password = user.getPassword();

        authorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(Role.ROLE_PREFIX + role.getCode()))
                .collect(Collectors.toSet());

        // TODO Change when Userlocking is implemented
        enabled = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return enabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return enabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
