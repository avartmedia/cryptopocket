package org.avartmedia.cryptopocket.backend.be.repositories;

import org.avartmedia.cryptopocket.backend.be.entities.InvestmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Christian on 15.12.2017.
 */
@Repository
public interface InvestmentRepository extends JpaRepository<InvestmentEntity, Long> {

    List<InvestmentEntity> findByUserId(Long userId);
}
