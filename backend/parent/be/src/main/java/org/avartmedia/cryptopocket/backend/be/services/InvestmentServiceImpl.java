package org.avartmedia.cryptopocket.backend.be.services;

import com.avartmedia.greencup.api.exception.EntityNotFoundException;
import com.avartmedia.greencup.api.exception.InternalServiceException;
import com.avartmedia.greencup.utils.date.JavaTimeUtils;
import org.apache.commons.lang3.Validate;
import org.avartmedia.cryptopocket.backend.api.dto.InvestmentDTO;
import org.avartmedia.cryptopocket.backend.api.dto.UserWealthStatisticsDTO;
import org.avartmedia.cryptopocket.backend.api.enums.InvestmentMode;
import org.avartmedia.cryptopocket.backend.api.services.InvestmentService;
import org.avartmedia.cryptopocket.backend.api.services.rest.CryptocompareService;
import org.avartmedia.cryptopocket.backend.be.entities.BalanceEntity;
import org.avartmedia.cryptopocket.backend.be.entities.InvestmentEntity;
import org.avartmedia.cryptopocket.backend.be.entities.User;
import org.avartmedia.cryptopocket.backend.be.repositories.BalanceRepository;
import org.avartmedia.cryptopocket.backend.be.repositories.InvestmentRepository;
import org.avartmedia.cryptopocket.backend.be.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by Christian on 14.12.2017.
 */
@Service
public class InvestmentServiceImpl implements InvestmentService {

    private final CryptocompareService cryptocompareService;
    private final UserRepository userRepository;
    private final BalanceRepository balanceRepository;
    private final InvestmentRepository investmentRepository;

    @Autowired
    public InvestmentServiceImpl(CryptocompareService cryptocompareService, UserRepository userRepository,
                                 BalanceRepository balanceRepository, InvestmentRepository investmentRepository) {
        this.cryptocompareService = cryptocompareService;
        this.userRepository = userRepository;
        this.balanceRepository = balanceRepository;
        this.investmentRepository = investmentRepository;
    }

    @Override
    public void createInvestment(Long userId, InvestmentDTO dto) {
        createInvestment(userId, dto, InvestmentMode.BUY);
    }

    @Override
    public void sellInvestment(Long userId, InvestmentDTO dto) {
        createInvestment(userId, dto, InvestmentMode.SELL);
    }

    @Override
    public UserWealthStatisticsDTO getTotalWealthByUser(Long userId) {
        Validate.notNull(userId, "UserId must be set.");

        BigDecimal networth = BigDecimal.ZERO;
        for (BalanceEntity b : balanceRepository.findByUserId(userId)) {
            networth = networth.add(cryptocompareService.getCurrencyValue(b.getCurrencyCode(), BigDecimal.valueOf(b.getAmount())));
        }

        BigDecimal totalInvestment = BigDecimal.ZERO;
        BigDecimal totalSoldInvestment = BigDecimal.ZERO;

        for (InvestmentEntity i : investmentRepository.findByUserId(userId)) {
            BigDecimal iPrice = BigDecimal.valueOf(i.getTotalPrice());

            InvestmentMode mode = InvestmentMode.getByCode(i.getMode());
            if (mode == InvestmentMode.BUY) {
                totalInvestment = totalInvestment.add(iPrice);
            } else {
                totalSoldInvestment = totalSoldInvestment.add(iPrice);
            }
        }

        return new UserWealthStatisticsDTO(totalInvestment, totalSoldInvestment, networth);
    }

    private void createInvestment(Long userId, InvestmentDTO dto, InvestmentMode mode) {
        if (mode == null) {
            throw new IllegalStateException("#createInvestment without Mode called.");
        }

        Validate.notNull(userId, "UserId must be set.");
        Validate.notNull(dto, "Dto must be set.");

        Validate.notNull(dto.getCurrencyCode(), "CurrencyCode must be set.");
        Validate.notNull(dto.getAmount(), "Amount must be set.");
        Validate.notNull(dto.getTotalPaid(), "TotalPaid must be set.");
        Validate.notNull(dto.getInvestmentDate(), "PaymentDate must be set.");

        User user = userRepository.getOne(userId);
        if (user == null) {
            throw new EntityNotFoundException(User.class, userId);
        }

        InvestmentEntity entity = new InvestmentEntity();
        entity.setUser(user);

        entity.setCurrencyCode(dto.getCurrencyCode());
        entity.setAmount(dto.getAmount().doubleValue());
        entity.setTotalPrice(dto.getTotalPaid().doubleValue());
        entity.setInvestmentDate(JavaTimeUtils.toDate(dto.getInvestmentDate()));

        entity.setMode(mode.getCode());

        investmentRepository.save(entity);

        // TODO move to BalanceServiceImpl
        BalanceEntity balance = balanceRepository.findByUserIdAndCurrencyCode(userId, dto.getCurrencyCode());

        if (mode == InvestmentMode.BUY) {
            if (balance == null) {
                balance = new BalanceEntity();
                balance.setUser(user);

                balance.setCurrencyCode(entity.getCurrencyCode());
            }

            balance.setAmount(getDoubleValueNullsafe(balance.getAmount()) + entity.getAmount());
            balance.setTotalPaymentPrice(getDoubleValueNullsafe(balance.getTotalPaymentPrice()) + entity.getTotalPrice());
        } else {
            if (balance == null) {
                // TODO Debug-Logs
                throw new InternalServiceException("No Balance found to sell.");
            }

            if (balance.getAmount() < entity.getAmount()) {
                throw new InternalServiceException("Not enough Balance found to sell.");
            }

            BigDecimal initialAmount = BigDecimal.valueOf(balance.getAmount());
            BigDecimal initialTotalPayment = BigDecimal.valueOf(balance.getTotalPaymentPrice());

            // TODO rounding?
            BigDecimal newTotalPayment = initialTotalPayment.divide(initialAmount, BigDecimal.ROUND_HALF_UP).multiply(dto.getAmount());

            balance.setAmount(balance.getAmount() - entity.getAmount());
            balance.setTotalPaymentPrice(newTotalPayment.doubleValue());
        }

        balanceRepository.save(balance);
    }

    private Double getDoubleValueNullsafe(Double d) {
        return d == null ? 0d : d;
    }
}
