package org.avartmedia.cryptopocket.backend.be.entities;

import com.avartmedia.greencup.be.entity.VersionedEntity;
import com.avartmedia.greencup.be.utils.EntityColumnLength;

import javax.persistence.*;

/**
 * Created by Christian on 14.12.2017.
 */
@Entity
@Table(
        name = "BALANCE",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"USER_FK", "CURRENCY_CODE"})
        }
)
public class BalanceEntity extends VersionedEntity<Long> {

    @Id
    @Column(name = "BALANCE_PK", unique = true, updatable = false, nullable = false)
    @SequenceGenerator(name = "BALANCE_SQ", sequenceName = "BALANCE_SQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BALANCE_SQ")
    private Long id;

    @Column(name = "CURRENCY_CODE", length = EntityColumnLength.CODE, updatable = false, nullable = false)
    private String currencyCode;

    @Column(name = "AMOUNT", nullable = false)
    private Double amount;

    @Column(name = "TOTAL_PAYMENT_PRICE", nullable = false)
    private Double totalPaymentPrice;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "USER_FK", updatable = false)
    private User user;

    @Override
    public Long getId() {
        return id;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getTotalPaymentPrice() {
        return totalPaymentPrice;
    }

    public void setTotalPaymentPrice(Double totalPaymentPrice) {
        this.totalPaymentPrice = totalPaymentPrice;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
