package org.avartmedia.cryptopocket.backend.be.idm.services;

import com.avartmedia.greencup.api.auth.TokenWrapper;
import org.apache.commons.lang3.Validate;
import org.avartmedia.cryptopocket.backend.api.idm.IdmUser;
import org.avartmedia.cryptopocket.backend.api.idm.services.AuthenticationService;
import org.avartmedia.cryptopocket.backend.api.idm.services.IdmService;
import org.avartmedia.cryptopocket.backend.api.idm.token.TokenManager;
import org.avartmedia.cryptopocket.backend.be.idm.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final IdmService idmService;
    private final TokenManager tokenManager;

    @Autowired
    public AuthenticationServiceImpl(IdmService idmService, TokenManager tokenManager) {
        this.idmService = idmService;
        this.tokenManager = tokenManager;
    }

    @Override
    public void validateToken(TokenWrapper token, WebAuthenticationDetails details) {
        Validate.notNull(token, "Token must be set.");

        String username = tokenManager.extractUsernameFromToken(token);
        Validate.notNull(username, "Username not found in Token " + token.getToken());

        IdmUser user = idmService.findByUsername(username);

        boolean tokenValid = tokenManager.isTokenValid(user.getId(), token);
        if (!tokenValid) {
            throw new AuthenticationServiceException("Token " + token.getToken() + " for User " + username +
                    " is not valid.");
        }

        UserDetails userDetails = new UserDetailsImpl(user);

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        authentication.setDetails(details);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Override
    public UserDetails getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (UserDetails) authentication.getPrincipal();
    }
}
