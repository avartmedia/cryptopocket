package org.avartmedia.cryptopocket.backend.be.repositories;

import org.avartmedia.cryptopocket.backend.be.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
