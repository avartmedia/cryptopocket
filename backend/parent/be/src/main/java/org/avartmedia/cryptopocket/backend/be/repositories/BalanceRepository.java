package org.avartmedia.cryptopocket.backend.be.repositories;

import org.avartmedia.cryptopocket.backend.be.entities.BalanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Christian on 14.12.2017.
 */
@Repository
public interface BalanceRepository extends JpaRepository<BalanceEntity, Long> {

    List<BalanceEntity> findByUserId(Long userId);

    BalanceEntity findByUserIdAndCurrencyCode(Long userId, String currencyCode);
}
